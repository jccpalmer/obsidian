# Analytics references and resources
Created: 2022-08-01 at 07:41

1. [4 Examples of Business Analytics in Action](https://online.hbs.edu/blog/post/business-analytics-examples)
2. [Data Science & Big Data Analytics](https://onlinelibrary.wiley.com/doi/book/10.1002/9781119183686)
3. [Managing the Analytics Life Cycle  
for Decisions at Scale](https://www.sas.com/content/dam/SAS/en_us/doc/whitepaper1/manage-analytical-life-cycle-continuous-innovation-106179.pdf)
4. [Understanding the data analytics project life cycle](http://pingax.com/understanding-data-analytics-project-life-cycle/)
5. [Big Data Adoption and Planning Considerations](https://www.informit.com/articles/article.aspx?p=2473128)
6. [Data Management Life Cycle](https://www.fws.gov/data/life-cycle)
7. [USGS Data Lifecycle](https://www.usgs.gov/data-management/data-lifecycle)
8. [8 Steps in the Data Life Cycle](https://online.hbs.edu/blog/post/data-life-cycle)
9. [SQL Tutorial](https://www.w3schools.com/sql/default.asp) ^3cc5fc
10. [SQL Cheat Sheet](https://towardsdatascience.com/sql-cheat-sheet-776f8e3189fa) ^681b59
11. [Tableau How-Tos](https://public.tableau.com/en-us/s/resources) ^a8fcba
12. [RStudio Cheat Sheets](https://www.rstudio.com/resources/cheatsheets/) ^6fb51e
13. [Beyond the Numbers: A Data Analyst Journey](https://www.youtube.com/watch?v=t2oOFs4WgI0) ^4e7d7f
14. [How 1 retailer’s data strategy powers seamless customer experiences](https://www.thinkwithgoogle.com/future-of-marketing/digital-transformation/crate-and-barrel-digital-customer-experiences/) ^502fc1
15. [How one of the world’s biggest marketers ripped up its playbook and learned to anticipate intent](https://www.thinkwithgoogle.com/marketing-strategies/data-and-measurement/pepsi-digital-transformation/) ^345e73
16. [compelling-dashboards.pdf](file:///home/jordan/Downloads/compelling-dashboards.pdf) ^d01320
17. [Real-World Examples of Business Intelligence (BI) Dashboards](https://www.tableau.com/learn/articles/business-intelligence-dashboards-examples) ^39dd79
18. [Requirements Gathering Worksheet.pdf](file:///home/jordan/Documents/Requirements%20Gathering%20Worksheet.pdf) ^9e0b7f
19. [Filter Actions](https://help.tableau.com/current/pro/desktop/en-us/actions_filter.htm) ^f7d0dd
20. [Spreadsheet Errors and Fixes](file:///home/jordan/Documents/Obsidian/Spreadsheet%20Errors%20and%20Fixes.pdf) ^eee04b
21. [US Government Data](https://www.data.gov/)
22. [US Census Bureau](https://www.census.gov/data.html)
23. [Open Data Network](https://www.opendatanetwork.com/)
24. [Google Cloud Public Datasets](https://cloud.google.com/public-datasets)
25. [Dataset Search](https://datasetsearch.research.google.com/)
26. [Excel video training](https://support.microsoft.com/en-us/office/excel-video-training-9bc05390-e94c-46af-a5b3-d7c22f6990bb)
27. [Google Cloud Public Datasets](https://cloud.google.com/public-datasets)
28. [Dataset Search](https://datasetsearch.research.google.com/)
29. [BigQuery](https://cloud.google.com/bigquery/public-data)
30. [Global Health Observatory data](https://www.who.int/data/collections)
31. [The Cancer Imaging Archive dataset](https://cloud.google.com/healthcare/docs/resources/public-datasets/tcia)
32. [1000 Genomes](https://cloud.google.com/life-sciences/docs/resources/public-datasets/1000-genomes)
33. [National Climatic Data Center](https://www.ncei.noaa.gov/products)
34. [NOAA Public Dataset Gallery](https://www.climate.gov/maps-data/datasets)
35. [UNICEF State of the World's Children](https://data.unicef.org/resources/dataset/sowc-2019-statistical-tables/)
36. [CPS Labor Force Statistics](https://www.bls.gov/cps/tables.htm)
37. [The Stanford Open Policing Project](https://openpolicing.stanford.edu/)
38. [[References/SQL Best Practices.pdf | SQL Best Practices]]
39. [Central Limit Theorem](https://www.investopedia.com/terms/c/central_limit_theorem.asp)
40. [Sample Size Formula](https://www.statisticssolutions.com/dissertation-resources/sample-size-calculation-and-sample-size-justification/sample-size-formula/)
41. [Automating Scientific Data Analysis](https://towardsdatascience.com/automating-scientific-data-analysis-part-1-c9979cd0817e)
42. [Automating Big Data Analysis](https://news.mit.edu/2016/automating-big-data-analysis-1021)
43. [10 Best Options for Workflow Automation Software](https://technologyadvice.com/blog/information-technology/top-10-workflow-automation-software/)
44. [What Is a SQL Dialect](https://learnsql.com/blog/what-sql-dialect-to-learn/)
45. [Differences Between SQL vs. MySQL vs. SQL Server](https://www.softwaretestinghelp.com/sql-vs-mysql-vs-sql-server/)
46. [SQL Server, PostgreSQL, MySQL... What's the difference?](https://www.datacamp.com/community/blog/sql-differences)
47. 