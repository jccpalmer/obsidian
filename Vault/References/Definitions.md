# Definitions
Created: 2022-05-27 at 07:18

## English words
1. imperious: assuming power or authority without justification; arrogant and domineering ^edfc4c
2. impugn: assuming power or authority without justification; arrogant and domineering ^6c2f38
3. superciliousness: the act of behaving or looking as though one thinks one is superior to others ^c42c55
4. bedlamite: an insane person, a lunatic; insane ^bd92b8
5. heuristic: enabling someone to discover or learn something for themselves
6. atavism: a tendency to revert to something ancient or ancestral ^80dfe0

## Philosophies
1. Stoicism: A philsophy that teaches that virtue, the highest good, is based on knowledge; the wise live in harmony with the divine Reason (also identified with Fate and Providence) that governs nature, and are indifferent to the vicissitudes of fortune and to pleasure and pain. ^4e229e
2. Nihilism: The rejection of all religious and moral principles; the belief that life is meaningless; extreme skepticism maintaining that nothing in the world has a real existence. ^becede
3. Positivism: A philosophical system that holds that every rationally justifiable assertion can be scientifically verified or is capable of logical or mathematical proof, and that therefore rejects metaphysics and theism.
4. Atomism: A philosophy that asserts that all physical objects consist of different arrangements of eternal atoms and the infinite void in which they form different combinations and shapes. ^1df4bf
5. Will to Power: Nietzsche's concept of the main driving force in humans and all life; though never defined, it is often thought to be self-determination and the actualization of the ego into the external world. ^8b2295

## Latin phrases
1. *causa prima*: first cause ^c323f9
2. *virtus dormitiva*: an explanation that restates the question in different words ^f43c9c
3. *a priori*: relating to or denoting reasoning or knowledge which proceeds from theoretical deduction rather than from observation or experience; in a way based on theoretical deduction rather than empirical observation: from what is before ^26b060
4. *reductio ad absurdum*: the form of argument that attempts to establish a claim by showing that the opposite scenario would lead to absurdity or contradiction
5. *causa sui*: a term that denotes something that is generated within itself; cause of itself, self-caused

## French phrases
1. bric-à-brac: an elaborate collection of curios