# Gaelic mythology inspirations
Created: 2022-03-08 at 09:53

Caltan gods: Midyir (Death), Dagdara (Fate), Brigit (Eternity)

Order sigil: Winged Hither, immortal bird, with crossed gavel and sword

Order patron deity: Nuada, Wielder of the White Light

Findrias, Cloud-Fair city, leads to Sharayim and lies in the Glanmor Vale
Murias, once Aursentorn and of deep waters, leads to the Moonside Lake
Gorias, now Saorise and Flame-Bright, leads to ?
Falias, city of steadfast adamant, lies in Inyadhon (?) and leads to Abbadon

Dagdara, goddess of Fate, has nine capes for the 9 Caltan realms (even though there are only four including the mortal plane); Hemmed in gold, silver, and purple thread

Cardinal Church = tripartite godhead; Caltan church = three head gods, seven demigods (with a fourth contested, Nuada)

The Caltan church keeps sheepskins as special items, since one was once thought to become a beautiful forest and another to bring a man the price of the skin and the skin itself. A certain type of wool is highly valued, said to have been given by the demi-god of the sea to one of the first Caltan settlers

Demi-goddess of dreams

Ethlinn, Lughmother, the mother of creation (a Caltan church version of Nuada's true nature)

## References
1. [[The Earth-Shapers]]
2. [[The Spear of Victory]]
3. [[A Good Action]]
4. [[How the Son of the Gobhaun Saor Sold the Sheepskin]]
5. [[How the Son of the Gobhaun Saor Shortened the Road]]
6. [[The Cow of Plenty]]