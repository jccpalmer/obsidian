# Data Analytics Module 3
Created: 2022-09-27 at 07:10

## Definitions
1. First-party data: Data collected by an individual or group using their own resources
2. Second-party data: Data collected by a group directly from its audience and then sold
3. Third-party data: Data collected from outside sources who did not collect it directly
4. Population: All possible data values in a certain dataset
5. Sample: A part of a population that is representative of the population 
6. Discrete data: Data that is counted and has a limited number of values
7. Continuous data: Data that is measured and can have almost any numeric value
8. Nominal data: A type of qualitative data that is categorized without a set order
9. Ordinal data: A type of qualitative data with a set order or scale
10. Internal data: Data that lives within a company's own systems
11. External data: Data that lives and is generated outside of an organization
12. Structured data: Data organized in a certain format such as rows and columns
13. Unstructured data: Data that is not organized in any easily identifiable manner
14. Data model: A model that is used for organizing data elements and how they relate to one another
15. Data elements: Pieces of information, such as names, account numbers, and addresses
16. Entity Relationship Diagram (ERD): A visual way to understand the relationship between entities in a data model
17. Unified Modeling Language (UML): Detailed diagrams that describe the structure of a system by showing the system's entities, attributes, operations, and their relationships
18. Data type: A specific kind of data attribute that tells what kind of value the data is
19. Text or string type: A sequences of characters and punctuation that contains textual information
20. Boolean type: A data type with only two possible values, such as TRUE or FALSE
21. Wide data: Data in which every data subject has a single row with multiple columns to hold the values of various attributes of the subject
22. Long data: Data in which each row is one time point per subject, so each subject will have data in multiple rows
23. Data transformation: The process of changing the data's format, structure, or values
24. Bias: A preference in favor of or against a person, group, or thing
25. Data bias: A type of error that systematically skews results in a certain direction
26. Sampling bias: When a sample isn't representative of the population as a whole
27. Unbiased sampling: When a sample is representative of the population being measured
28. Observer bias: The tendency for different people to observe things differently
29. Interpretation bias: The tendency to always interpret ambiguous situations in a positive or negative way
30. Confirmation bias: The tendency to search for or interpret information in a way that confirms pre-existing beliefs
31. Ethics: Well-founded standards of right and wrong that prescrive what humans ought to do, usually in terms of rights, obligations, benefits to society, fairness, or specific virtues
32. Data ethics: Well-founded standards of right and wrong that dictate how data is collected, shared, and used
33. GDPR: General Data Protection Regulation of the EU
34. Openness (open data): Free access, usage, and sharing of data
35. Availability and access: Open data must be available as a whole
36. Reuse and Redistribution: Open data must be available under terms that allow reuse and redistribution
37. Universal Participation: Everyone must be able to use, reuse, and redistribute the data
38. Data interoperability: The ability of data systems and services to openly connect and share data
39. Database: A collection of data stored in a computer system
40. Metadata: Data about data
41. Relational database: A database that contains a series of related tables that can be connected via their relationships
42. Primary key: An identifier that references a column in which each value is unique
43. Foreign key: A field within a table that is a primary key in another table
44. Normalization: A process of organizing data in a relational database
45. Composite key: A primary key constructed using multiple columns of a table
46. Descriptive metadata: Metadata that describes a piece of data and can be used to identify it at a later point in time
47. Structural metadata: Metadata that indicates how a piece of data is organized and whether it is part of one, or more than one, data collection
48. Administrative metadata: Metadata that indicates the technical source of a digital asset
49. Metadata repository: A database specifically created to store metadata
50. Data governance: A process to ensure the formal management of a company's data assets
51. Sorting data: Arranging data into a meaningful order to make it easier to understand, analyze, and visualize
52. Filtering: Showing only the data that meets a specific criteria while hiding the rest
53. Naming conventions: Consistent guidelines that describe the content, date, or version of a file in its name
54. Data security: Protecting data from unauthorized access or corruption by adopting safety measures
55. Encryption: The use of a unique algorithm to alter data and make it unusable by users and applications that don't know the algorithm
56. Tokenization: The replacement of data elements with randomly generated data referred to as tokens

## Concepts

## Reflections

## References
1. [[Data Analytics Certification Module 3 Week 1]]
2. [[Data Analytics Certification Module 3 Week 2]]
3. [[Data Analytics Certification Module 3 Week 3]]
4. [[Data Analytics Certification Module 3 Week 4]]
5. [[Data Analytics Certification Module 3 Week 5]]