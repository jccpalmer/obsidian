# Data Analytics Module 2
Created: 2022-09-21 at 14:24

## Definitions

1. Structured thinking: The process of recognizing the current problem or situation, organizing the available information, revealing gaps and opportunities, and identifying the options ^c96a81
2. Fairness: Ensuring questions don't create or reinforce bias ^7fd01a
3. Leading question: Leading a person to answer a certain way ^24d1e0
4. Close-ended question: The only answers are "yes" and "no"
5. Vague question: A question that lacks context or meaning
6. Algorithm: A process or set of rules to be followed for a specific task
7. Quantitative data: Specific and objective measures of numerical facts; quantity ^1a6425
8. Qualitative data: Subjective or explanatory measures of qualities and characteristics; quality ^07b44c
9. Report: A static collection of data given to stakeholders periodically ^f26be4
10. Dashboard: A monitor for live, incoming data ^3893ec
11. Pivot table: A data summarization tool that is used to summarize, sort, reorganize, group, count, total, or average data stored in a database or spreadsheet
12. Metric: Single, quantifiable type of data that can be used for measurement ^6caae1
13. Metric goal: A measurable goal set by a company and evaluated using metrics ^dea5bb
14. Mathematical thinking: Looking at a problem and logically breaking it down step by step to see the relationship and patterns in the data ^0e5a0a
15. Formula: A set of instructions that performs a specific calculation ^de4417
16. Operator: A symbol that names the type of operation or calculation ot be performed ^a0d403
17. Cell reference: A cell or range of cells in a worksheet that can be used in a formula ^589ea0
18. Range: A collection of two or more cells
19. Function: A preset command that automatically performs a specific process or task using the data ^24d896
20. Problem domain: The specific area of analysis that encompasses every activity affecting or affected by the problem
21. Scope of work (SOW): An agreed-upon outline of the work you're going to perform on a project ^d6dc0b
22. Stakeholders: People that have invested time, interest, and resources into the projects analysts work on ^bc4244
23. Turnover rate: The rate at which employees leave a company
24. Data-inspired decision-making: Exploration of different data sources to find out what they have in common

## Concepts

### Structured thinking & asking questions 
- Effective problem-solving and questioning require [[Data Analytics Module 2#^c96a81 | structured thinking]].
	- When data is poorly analyzed or misinterpreted, it can lead to disastrous results.
	- Conversely, when data is used strategically, businesses can transform and grow their revenue.
- It is important for you to keep a data-driven mindset, ask lots of questions, experiment with many different possibilities, and use both logic and creativity along the way. You will then be prepared to interpret your data with the highest levels of care and accuracy.
* There is a key difference between making a decision with incomplete data versus a small dataset.
- When starting on a project, it's important to remember the six steps of data analysis in order to take action with data:
	- Ask
		- It’s impossible to solve a problem if you don’t know what it is. These are some things to consider:
		* Define the problem you’re trying to solve
		* Make sure you fully understand the stakeholder’s expectations
		* Focus on the actual problem and avoid any distractions
		* Collaborate with stakeholders and keep an open line of communication
		* Take a step back and see the whole situation in context
	* Questions to ask:
		* What are my stakeholders saying their problems are?
		* Now that I've identified the issues, how can I help the stakeholders resolve their questions?
	* Prepare
		* You will decide what data you need to collect in order to answer your questions and how to organize it so that it is useful. You might use your business task to decide:
			* What metrics to measure
			* Locate data in your database
			* Create security measures to protect that data
		* Questions to ask:
			* What do I need to figure out how to solve this problem?
			* What research do I need to do?
	* Process
		* Clean data is the best data and you will need to clean up your data to get rid of any possible errors, inaccuracies, or inconsistencies. This might mean:
			* Using spreadsheet functions to find incorrectly entered data
			* Using SQL functions to check for extra spaces
			* Removing repeated entries
			* Checking as much as possible for bias in the data
		* Questions to ask:
			* What data errors or inaccuracies might get in my way of getting the best possible answer to the problem I am trying to solve?
			* How can I clean my data so the information I have is more consistent?
	* Analyze
		* You will want to think analytically about your data. At this stage, you might sort and format your data to make it easier to:
			* Perform calculations
			* Combine data from multiple sources
			* Create tables with your results
		* Questions to ask:
			* What story is my data telling me?
			* How will my data help me solve this problem?
			* Who needs my company's product or service? What type of person is most likely to use it?
	* Share
		* Everyone shares their results differently so be sure to summarize your results with clear and enticing visuals of your analysis using data via tools like graphs or dashboards. This is your chance to show the stakeholders you have solved their problem and how you got there. Sharing will certainly help your team:
			* Make better decisions
			* Make more informed decisions
			* Lead to stronger outcomes
			* Successfully communicate your findings
		* Questions to ask:
			* How can I make what I present to the stakeholders engaging and easy to understand?
			* What would help me understand this if I were the listener?
	* Act
		* Now it’s time to act on your data. You will take everything you have learned from your data analysis and put it to use. This could mean providing your stakeholders with recommendations based on your findings so they can make data-driven decisions.
			* Questions to ask:
				* How can I use the feedback I received during the Share phase to actually meet the stakeholder’s needs and expectations?
* When asking questions, always make sure that they are SMART.
	* Specific
		* Simple, significant, and focused on a single topic or a few closely-related ideas
	* Measurable
		* Can be quantified and assessed
	* Action-oriented
		* Encourage change
	* Relevant
		* Matter, are important, and have significance to the problem
	* Time-bound
		* Specify the time to be studied
* SMART questions need to be [[Data Analytics Module 2#^7fd01a | fair]]. This ensures that they ask for the right answers, and it also saves time when analyzing the dataset. 
	* Unfair questions can make the job more difficult thanks to unreliable feedback and missed opportunities for insight
	- [[Data Analytics Module 2#^24d1e0 | Leading questions]] and questions that make assumptions are also unfair
	* Questions need to clear with wording that anyone can understand

### Working with data
* Working with data will bring up all sorts of problems, so the most common solutions to problems are:
	1. Making predictions
		* Using data to make an informed decision about how things may be in the future
	2. Categorizing things
		* Assigning information to different groups of clusters based on common features
	3. Spotting something unusual
		* Identifying data that is different from the norm
	4. Identifying themes
		* Grouping categorized information into broader concepts; takes categories a step further by grouping them into broader themes
	5. Discovering connections
		* Finding similar challenges faced by different entities and combining data and insights to address them
	6. Finding patterns
		* Using historical data to understand what happened in the past and is therefore likely to happen again
* Both [[Data Analytics Module 2#^07b44c | qualitative data]] and [[Data Analytics Module 2#^1a6425 | quantitative data]] have their respective places in the data analysis workflow. 
	* Quantitative data can be visualized with charts and graphs; qualtitative data can give a higher level understanding of why the numbers are the way they are.
* Quantitative data tools
	* Focus groups
	* Social media text analysis
	* In-person interviews
* Qualitative data tools
	* Structured interviews
	* Surveys
	* Polls

### Contextualizing the data
- [[Data Analytics Module 2#^f26be4 | Reports]]
	* Pros
		* High-level historical data
		* Easy to design and use
		* Pre-cleaned and sorted data
	* Cons
		* Continual maintenance
		* Less visually appealing
		* Static
* [[Data Analytics Module 2#^3893ec | Dashboards]]
	* Pros
		* Dynamic, automatic, and interactive
		* More stakeholder access
		* Low maintenance
		* More visually appealing
	* Cons
		* Labor-intensive design
		* Can be confusing and less efficient
		* Potentially uncleaned data
* Dashboards benefit the following parties:
	* Analysts
		* Centralization: Sharing a single source of data with all stakeholders
		* Visualizaton: Showing and updating live, incoming data in real time
		* Insightfulness: Pulling relevant info from different datasets
		* Customization: Creating custom views dedicated to a specific person, project, or presentation of the data
	* Stakeholders
		* Centralization: Working with a comprehensive view of data, initiatives, objectives, projects, processes, and more
		* Visualization: Spotting changing trends and patters quicker
		* Insightfulness: Understanding the story behind the numbers to keep track of goals and make data-driven decisions
		* Customization: Drilling down to more specific areas of specialized interest or concern
* Creating a dashboard:
	1. Identify the stakeholders who need to see the data and how they will use it
		1. Ask effective questions
	2. Design the dashboard
		1. Use a clear header to label the information
		2. Add short text descriptions to each visualization
		3. Show the most important information at the top
	3. Create mock-ups if desired
	4. Select the visualizations you will use on the dashboard
		1. Line/bar graphs: Change in values over time
		2. Pie/donut: How each part contributes to the whole
	5. Create filters as needed
		1. Filters show 
* [[Data Analytics Module 2#^6caae1 | Metrics]], which typically involve some small amount of math, are vital to the success of a data analysis project. Data often contains a lot of perspectives to be explored, so it's important to know what you're looking for before you set out.
- Understanding metrics leads into developing the [[Data Analytics Module 2#^dea5bb | metric goals]] required for the project.

### Connecting the data dots
- [[Data Analytics Module 2#^0e5a0a | Mathematical thinking]] helps to determine the best tools to use for the project.
- When deciding on a tool, consider the following:
	* Size of dataset
		* Small data = use spreadsheets
			* Describes a dataset made up of specific metrics of a short, well-defined time period
			* Usually organized and analyzed in spreadsheets
			* Day-to-day decisions
			* Likely to be used by small and midsize businesses
			* Simple to collect, store, manage, sort, and visually represent
			* Usually already a manageable size for analysis
		* Big data = databases/SQL
			* Describes large, less-specific datasets that cover a long time period
			* Usually kept in a database and queried
			* Likely to be used by large organizations
			* Takes a lot of effor to collect, store, manage, sort, and visually represent
			* Usually needs to be broken into smaller pieces in order to be organized and analyzed effectively decision making
* Big data has a lot of advantages, but there are also challenges:
	*  A lot of organizations deal with data overload and way too much unimportant or irrelevant information.
	* Important data can be hidden deep down with all of the non-important data, which makes it harder to find and use. This can lead to slower and more inefficient decision-making time frames.
	* The data you need isn’t always easily accessible.
	* Current technology tools and solutions still struggle to provide measurable and reportable data. This can lead to unfair algorithmic bias.
	* There are gaps in many big data business solutions.
- Despite the challenges, big data has the following benefits:
	- When large amounts of data can be stored and analyzed, it can help companies identify more efficient ways of doing business and save a lot of time and money.
	- Big data helps organizations spot the trends of customer buying patterns and satisfaction levels, which can help them create new products and solutions that will make customers happy.
	- By analyzing big data, businesses get a much better understanding of current market conditions, which can help them stay ahead of the competition.
	- Bbig data helps companies keep track of their online presence—especially feedback, both good and bad, from customers. This gives them the information they need to improve and protect their brand.
- Four V words for big data:
	- Volume: The amount of data
	- Variety: The different kinds of data
	- Velocity: How fast the data can be processed
	- Veracity: The quality and reliability of the data

### Spreadsheets
- Analysts use functions and formulae to perform calculations in spreadsheets.
- Spreadsheets and the data life cycle:
	- **Plan** for the users who will work within a spreadsheet by developing organizational standards.
		- Formatting cells
		- Highlighting headings
		- Color scheme
		- Way data is ordered
	- **Capture** data by the source by connecting spreadsheets to other data sources.
		- Data will be automatically updated in the spreadsheet
	- **Manage** different kinds of data with a spreadsheet; allows for deciding who can access the data, how the info is shared, and how to keep the data safe and secure.
		- Storing
		- Organizing
		- Filtering
		- Updating
	- **Analyze** data in a spreadsheet to help make better decisions.
		- Common tools
			- Formulae to aggregate data or create reports
			- Pivot tables for clear visuals
	- **Archive** any spreadsheet not often used that might need to be referenced later with built-in tools.
		- Useful for storing historical data before it gets updated
	- **Destroy** the spreadsheet when it is no longer needed, if there are better backup copies, or for legal/security reasons.
- [[Data Analytics Module 2#^de4417 | Formulae]] and [[Data Analytics Module 2#^24d896 | functions]] are how an analyst interacts with spreadsheets.
	- Both require an [[Data Analytics Module 2#^a0d403 | operator]] to work.
		- Common operators:
			- + for addition
			- - for subtraction
			- * for multiplication
			- / for division
	- Start each expression with the = sign.
- [[Data Analytics Module 2#^589ea0 | Cell references]] are how the functions and formulae are calculated.
	- Relative references change depending on the context, often when the formula or function is copied and pasted into another cell.
	- Absolute references do not change and are marked with the $ sign.
- Common errors you might come across in working with spreadsheets:
	- #DIV/0!: A formula is trying to divide a value in a cell by 0 or by an empty cell
	- IFERROR(TK:TK, "N/A")
	- #ERROR! (Google Sheets only): A formula can't be interpreted as input (parsing error)
		- Often because of a missing delimiter
	- #N/A: Data in a formula can't be found by the spreadsheet
	- #NAME?: A formula or function name isn't understood
	- #NUM!: A formula or function calculation can't be performed as specified
	- #VALUE!: A general error that could indicate a problem with a formula or referenced cells
	- #REF!: A formula is referencing a cell that is no longer valid or has been deleted
- Best practices when working with spreadsheets:
	- Filter data to make your spreadsheet less complex and busy.
	- Use and freeze headers so you know what is in each column, even when scrolling.
	- When multiplying numbers, use an asterisk not an X.
	- Start every formula and function with an equal sign (=).
	- Whenever you use an open parenthesis, make sure there is a closed parenthesis on the other end to match.
	- Change the font to something easy to read.
	- Set the border colors to white so that you are working in a blank sheet.
	- Create a tab with just the raw data, and a separate tab with just the data you need.
- Conditional formatting can be used to highlight cells a different color based on their contents. This is useful for conveying information.

### Structured thinking & communication
- Think structurally and define the problem before trying to solve it.
- A [[Data Analytics Module 2#^d6dc0b | scope of work]] is composed of:
		- Deliverables
			- What work is being done, and what things are being created as a result of this project? When the project is complete, what are you expected to deliver to the stakeholders? Be specific here. Will you collect data for this project? How much, or for how long?
		- Timeline
			- Your timeline will be closely tied to the milestones you create for your project. The timeline is a way of mapping expectations for how long each step of the process should take. The timeline should be specific enough to help all involved decide if a project is on schedule. When will the deliverables be completed? How long do you expect the project will take to complete? If all goes as planned, how long do you expect each component of the project will take? When can we expect to reach each milestone?
		- Milestones
			- This is closely related to your timeline. What are the major milestones for progress in your project? How do you know when a given part of the project is considered complete?
		- Reports
			- Good SOWs also set boundaries for how and when you’ll give status updates to stakeholders. How will you communicate progress with stakeholders and sponsors, and how often? Will progress be reported weekly? Monthly? When milestones are completed? What information will status reports contain?
	- Not to be confused with *statement of work*
- SOWs should also contain information specific to what is and isn’t considered part of the project.
- Figuring out what data means is just as important as collecting it.
- There is no universal set of contextual interpretations.
	- Context is personal
- Start with an accurate representation of the data and collect it in the most objective way possible.
- [[Data Analytics Module 2#^bc4244 | Stakeholders]] play a massive role in the project, of which data analysis may only be a part. Common stakeholder groups include:
	- Executive team
		- The executive team provides strategic and operational leadership to the company. They set goals, develop strategy, and make sure that strategy is executed effectively. The executive team might include vice presidents, the chief marketing officer, and senior-level professionals who help plan and direct the company’s work. These stakeholders think about decisions at a very high level and they are looking for the headline news about your project first.  They are less interested in the details.
	- Customer-facing team
		- The customer-facing team includes anyone in an organization who has some level of interaction with customers and potential customers. Typically they compile information, set expectations, and communicate customer feedback to other parts of the internal organization. These stakeholders have their own objectives and may come to you with specific asks.
	- Data science team
- Working with stakeholders can be tricky, so keep the following in mind when working with them:
	- Discuss goals
	- Feel empowered to say "no"
	- Plan for the unexpected
	- Know your project
	- Start with words and visuals
	- Communicate often
- There also plenty of questions to ask regarding your stakeholders, such as:
	- Who are the primary and secondary stakeholders?
	- Who is managing the data?
	- Where can you go for help?
- When practicing healthy workplace communication, it's important to remember the following:
	- Who your audience is
	- What they already know
	- What they need to know
	- How you can communicate that effectively to them
- Flag problems early for stakeholders and set realistic expectations at every stage of the project.
- Balance speed with accuracy. Do not rush, but don't cause unnecessary delays, either.
	- The fastest answer is not the always the accurate one.
- When faced with conflict, reframe the question, then outline the problems and solutions.
- Data can, of course, face limitations, which you will need to communicate to your team and stakeholders. They are:
	- Incomplete or nonexistent
	- Misaligned
	- Dirty
- In order to tell a clear story, do the following:
	- Compare the same types of data
	- Visualize with care
	- Leave out needless graphs
	- Test for statistical significance
	- Pay attention to sample size
- Meetings are inevitable and are sometimes valuable. Here are some dos and don'ts
	- Meeting dos
		- Come prepared
			- Bring what you need
			- Read the meeting agenda
			- Prepare notes and presentations
			- Be ready to answer questions
		- Be on time
		- Pay attention
		- Ask questions
	- Meeting don'ts
		- Show up unprepared
		- Arrive late
		- Be distracted
		- Dominate the conversation
		- Talk over others
		- Distract people with unfocused discussion

## Reflections
1. Structured and mathematical thinking are vital to success as a data analyst. Deciding on whether to use small or big data is also important.
2. Much like deciding on the tools to use when analyzing data, choosing the right avenue for showing it is just as important. Reports and dashboards have their respective places, so it's key to pick the right one for the job.
3. Quantitative data excels at showing things visually, while qualitative data works best at showing a higher-level understanding of the data at large.
4. When asking questions, they need to be SMART and fair. Unfair, leading, and vague questions lead to unclear data, which can skew results and analysis.
5. It is important for you to keep a data-driven mindset, ask lots of questions, experiment with many different possibilities, and use both logic and creativity along the way. You will then be prepared to interpret your data with the highest levels of care and accuracy.

## References
1. [[Data Analytics Certification Module 2 Week 1]]
2. [[Data Analytics Certification Module 2 Week 2]]
3. [[Data Analytics Certification Module 2 Week 3]]
4. [[Data Analytics Certification Module 2 Week 4]]
5. [[Analytics references and resources]]