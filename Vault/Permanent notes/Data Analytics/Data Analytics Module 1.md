# Data Analytics Module 1
Created: 2022-08-03 at 08:05

## Definitions
1. Data: A collection of facts ^0d8b60
2. Data analysis: The collection, transformation, and organization of data to draw conclusions, make predictions, and drive informed decision making ^c5b258
3. Data ecosystems: The various elements that interact with one another in order to produce, manage, store, organize, analyze, and share data
4. Data science: Creating new ways of modeling and understanding the unknown using raw data
5. Data-driven decision making: Using facts to guide business strategy ^49c53c
6. Dataset: A collection of data that can be manipulated or analyzed as one unit
7. Analytical skills: Qualities and characteristics associated with solving problems using facts
8. Context: The condition in which something exists or happens^1739fb
9. Curiosity: Wanting to learn something, seeking out new challenges and experiences ^49102b
10. Technical mindset: The ability to break down the things into smaller steps or pieces and work with them in an orderly and logical way ^3b38fa
11. Data design: How one organizes information, such as with a database or phone contacts ^83cb88
12. Data strategy: The management of the people (the ones who know how to use the right data to find solutions), processes (or ensuring that the path to that solution is clear and accessible), and tools used in data analysis (the right technology is being used for the job) ^ef0aae
13. Visualization: The graphical representation of information ^98586a
14. Strategizing: The process of seeing the goals and possible processes with understanding the data; improves the quality and usefulness of the data collected
15. Root cause: The reason why a problem occurs ^d9a69c
16. Gap analysis: Allows the examination and evaluation of how a process works currently to get to a long-term future goal ^fadda6
18. Database: A collection of information stored inside a computer system ^defc08
19. Formula: A set of instructions that performs a specific calculation using the data in a spreadsheet ^bd2d21
20. Function: A preset command that automatically performs a process or task using the data in a spreadsheet
21. Query language: A computer programming language that allows for the retrieval and manipulation of data from a database ^6085e1
22. Attribute: A characteristic or quality of data used to label a column in a table
23. Observation: All of the attributes for something contained in a row of a data table
24. Query: A request for data or information from a database ^310382
25. Syntax: The predetermined structure of a language that includes all required words, symbols, and punctuation, as well as their proper placement ^31d3d1
26.  Business task: The question or problem that data analysis answers for a business
27. Issue: A topic or subject to investigate
28. Question: A statement designed to discover information
29. Problem: An obstacle or complication that needs to be worked out
30. Fairness: Ensuring that the analysis does not create or reinforce bias ^a90354
31. Beneficence: the conept of keeping the welfare of the subjects in mind; the quality or state of doing or producing good

## Concepts
### Data analysis basics
* [[Data Analytics Module 1#^c5b258 | Data analytics]] can help organizations completely rethink something they do or point them in a totally new direction.
	* Data analytics is the science of [[Data Analytics Module 1#^0d8b60 | data]].
	* A data analyst finds data, analyzes it, and uses it to uncover trends, patterns, and relationships.
	* Data provides a complete picture of the problem and its causes
		* This allows for the discovery of new and surprising solution that wouldn't have been possible to see before
* Data is data
* Six steps of data analysis:
	* Ask questions and define the problem.
	* Prepare data by collecting and storing the information.
	* Process data by cleaning and checking the information.
	* Analyze data to find patterns, relationships, and trends.
	* Share data with your audience.
	* Act on the data and use the analysis results.
		* Six steps apply to any data analysis.
* Ask
	* Define the problem to be solved.
		* Look at the current state and identify how it's different from the ideal state.
	* Understand stakeholder expectations.
		* Determine who the stakeholders are.
		* Communicating with stakeholders is key with staying on task.
	* Stakeholders: People who have invested time and resourced into a project and are interested in the outcome.
	* Ask all of the right questions at the beginning of the engagement so that you can better understand what your leaders and stakeholders need from this analysis.
		* “What is the problem that we are trying to solve,” “What is the purpose of this analysis,” and “What are we hoping to learn from it?”
* Prepare
	* Collect and store data for the analysis project.
	* Data and results should be objective and unbiased.
	* We need to think about what type of data we need to answer those key questions.
* Process
	* FInd and eliminate any errors or inaccuracies in the data.
		* Cleaning, removing outliers, transforming into a more useful format, combining two or more datasets to make information complete.
	* Check data the make sure it's complete and correct.
* Analyze
	* Using tools to transform and organize the information in order to draw conclusions, make predictions, and drive informed decision making.
	* This involes using spreadsheets and SQL.
	* Data analysts are trained to look for patterns, but the data are not our story to tell; this is the point where we must take a step back and let the data speak for themselves.
		* We might have a sneaking suspicion as to what the data will tell us.
* Share
	* This is how analysts interpret results and share them with others.
	* Visualization is an analyst's best friend.
	* Use slideshows and be prepared to answer questions.
* Act
	* This is where the business acts on the analyst's analysis.
* Questions to ask to find a balance between data and intuition.
	* What kind of results are needed?
	* Who will be informed?
	* Am I answering the question being asked?
	* How quickly does a decision need to be made? 
* Data analysts are responsible for the ethical collection and use of data.
	* To be ethical, one step to take is to ensure that a minimum amount of people have access to the raw data.
		* Similarly, ensure that only relevant parties get access to the analysis and aggregate data.
	* Data analysts have an obligation to ensure that their analysis is [[Data Analytics Module 1#^a90354 | fair]] and ethical.
	* Analysts need to create systems that are fair and inclusive.
		* Sometimes conclusions based on data can be both true *and* unfair.
		* Cherrypicking data can lead to erroneous conclusions, especially if someone is using inductive reasoning to reach said conclusions.
		* People should be able to consent to, or revoke consent of, collection of their data
		* Analysts should empower people to have control over their data
*  Sometimes conclusions based on data can be both true *and* unfair
	* E.g. an ethical data analyst can look at the data gathered and conclude that the company culture is preventing employees from succeeding
	* Cherrypicking data can lead to erroneous conclusions, especially if someone using inductive reasoning to reach said conclusions
* When it comes to data analytics, it's not just about minimizing harm but the concept of beneficence
* The excellence of statistics is rigor; the excellence of an analyst is speed; performance is one of the machine learning and artificial intelligence engineers’ goals.
* Data analysts can tap into data to gain valuable insights, verify their theories or assumptions, better understand opportunities and challenges, support an objective, and make a plan.
	* Data analysts use [[Data Analytics Module 1#^1739fb | context]] to make predictions, research answers, and draw conclusions.
* Sometimes the data-driven strategy will build on what has worked in the past.
	* Other times, it can guide a business to branch out in a whole new direction.
* There are five essential aspects to analytical skills:
	1. [[Data Analytics Module 1#^49102b | Curiosity]]
	2. Understanding: Context is crucial
	3. [[Data Analytics Module 1#^3b38fa | Technical mindset]]
	4. [[Data Analytics Module 1#^83cb88 | Data design]]
	5. [[Data Analytics Module 1#^ef0aae | Data strategy]]
* Analytical thinking involves identifying and defining a problem before solving it.
	* Its five key aspects are [[Data Analytics Module 1#^98586a | visualization]], strategy, problem-orientation, correlation, and big-picture and detail-oriented thinking.
	* To solve a problem, we use data in an organized, step-by-step manner.
* Data analysts use a technical mindset to seek out the facts, put them to work through analysis, and using the insights to make informed decisions.
* Data design has a strong connection to data-driven decision making
	* Designing data such that it is organized in a logical way makes it easier for other analysts to access, understand, and make the most of said data
	* Not just applicable to databases
* A data strategy incorporates the people, processes, and tools used to solve a problem
	* It offers a high-level view of the path an analyst should take to achieve their goals
* Data analysts use a problem-oriented approach to identify, describe, and solve problems.
	* They could identify correlations between two or more pieces of data.
		* Correlation does not equal causation! 
			* But it could indicate a relationship.
	* Analysts need and love to ask a lot of questions.
* All sorts of business problems benefit from both big picture and detail-oriented thinking.
	* Big picture thinking involves zooming out and seeing possibilities and opportunities.
	* Detail-oriented thinking is about figuring out all aspects that help with plan execution.
* Versatility is key to data analysis.
	* The more ways in which I can think, the more out-of-the-box solutions I can come up with.
* Always ask yourself: What is the [[Data Analytics Module 1#^d9a69c | root cause]]?
* Data analysis has an informal method of getting to the root cause called the Five Whys.
	* Ask "why?" five times to reveal the root cause.
		* The fifth answer should give useful, and sometimes surprising, insights.
* A data analyst should consider the gaps in the analysis process on a given subject, usually by conducting a [[Data Analytics Module 1#^fadda6 | gap analysis]].
	* The point is to understand where you are now and where you want to be later.
	* Ask yourself:
		* What did we not consider before?
		* What information or procedure might be missing from a process?
* In business, [[Data Analytics Module 1#^49c53c | data-driven decision-making]] can improve results in different ways.
* Data-driven decision-making increases the confidence in the decision and the ability to address business challenges.
	* It helps an analyst to become more proactive when an opportunity presents itself, and it saves time and effort when working toward a goal.
* It is important the ensure that specific procedures are in place so that everyone is on board and on the same page.

### Data analysis tools
* Data analysis tools include:
	* Spreadsheets
	* [[Data Analytics Module 1#^defc08 | Databases]]
	* Query languages (SQL)
	* Visualization software (Tableau)
* The stages in the data life cycle help to understand the individual phases that data goes through before starting the project analysis.
* Data life cycle:
	* Plan
		* What kind of data is needed?
		* How will the data be managed throughout its life cycle?
		* Who will be responsible for the data?
		* What are the optimal outcomes?
		* Happens before an analysis project
	* Capture
		* Collect the data from a variety of sources and bring it into the organization
	* Manage
		* How the data is cared for
		* How and where the data is stored
		* The tools used to keep it safe and secure
		* The actions taken to make sure it's maintained properly
		* Important to data cleansing
	* Analyze
		* The data is used to solve problems, make great decisions, support business goals
	* Archive
		* Store the data in a safe place where it is still available, but may not be used again
	* Destroy
		* Important for protecting a company's private information, as well as private data about customers
* Data analysis isn't a life cycle, it's the process of analyzing data.
* The most common tools you will see analysts use are spreadsheets, query languages, and visualization tools.
	* These tools allow analysts to be more focused on maximizing everything the former could do, streamlining their reporting, and just making their work simpler.
	* Popular spreadsheet programs are Excel and Sheets.
		* Spreadsheets store, organize, and sort data.
		* Data usefulness depends on how it's structured.
		* Spreadsheets let you identify patterns and piece the data together in a way that works for each specific data project.
			* As well as create excellent data visualizations, like graphs and charts.
* Spreadsheets:
	* Software applications
	* Structure data in a row/column format
	* Organize information in cells
	* Provide access to a limited amount of data
	* Manual data entry
	* Generally one user at a time
	* Controlled by the user
		* [[Data Analytics Module 1#^bd2d21 | Formulae]] can do basic things, such as add, subtract, multiply, and divide, but they do not stop there.
* Databases:
	* Data stores -- access using a [[Data Analytics Module 1#^6085e1 | query language]]
	* Structure data using rules and relationships
	* Organize information in complex collections
	* Provide access to huge amounts of data
	* Strict and consistent data entry
	* Multiple users
	* Controlled by a database management system
* Tableau is a popular visualization tool because it helps create visuals that are easy to understand
	* Simple drag-and-drop feature lets users create interactive graphs ain dashboards and worksheets
* Looker can give stakeholders a complete picture of the work by showing them visualization data and the actual data related to it
	* Communicates directly with a database, allowing for the connection of the data to the visual tool of choice

#### SQL
* Structured Query Language (SQL) is a data analysis tool using a query language.
	* Common SQL tools include MySQL, MS SQL Server, and Big Query.
* SQL allows analysts to isolate specific information from a database; make it easier for them to learn and understand the requests made to databases; and allow them to select, create, add, or download data from a database for analysis.
* SQL can do most things a spreadsheet can do, just more efficiently and with larger datasets.
* SQL [[Data Analytics Module 1#^310382 | queries]] are universal.
* SQL [[Data Analytics Module 1#^31d3d1 | syntax]] is the same:
	* SELECT: Choose the columns you want to return.
		* Enter the table columns.
	* FROM: Choose the tables where the columns you want are located.
		* Enter the table name.
	* WHERE: Filter for certain information.
		* Add the conditions for the query.
		* WHERE uses 'AND' (as well as 'OR' or 'NOT') statement to connect conditions, whereas SELECT uses commas.
			* LIKE clause is powerful because it allows for telling the database to look for a certain pattern.
		* EX:
		```
		SELECT
			customer_id,
			first_name,
			last_name
		FROM
			customer_data.customer_name
		WHERE
			customer_id > 0
			AND first_name = 'Jordan'
			AND last_name = 'Palmer'
		```
* The % is used as a wildcard to match one or more characters.
	* Some databases use * as a wildcard.
* Comments can also be added outside of a statement as well as within one.
	* This flexibility allows you to provide an overall description of the action, step-by-step notes about how to achieve it, and why you set different paramets or conditions.
	* Use either:
		```
		/* */
		---
		```
	* Do not rely on # for comments.
		* MySQL for example doesn't recognize #.
		* This isn't bash, YAML, etc.
	* Typically go for -- for comments.
* Aliases assign a new name to the columns or table to make them easier to work with and avoid the need for comments.
	* Uses the AS clause
	* Good for the duration of the query only
	* Doesn't change the actual name of a column or table in the database
	```
	field1 AS last_name
	table AS customers
	
	SELECT
		last_name
	FROM
		customers
	WHERE
		last_name LIKE 'Ch%';
	```
* <> = does not equal

#### Visualizations
* Visualizations make data easier to digest for an analyst's audience.
* Data visualization steps:
	1. Explore the data for patterns
	2. Plan the visuals
	3. Create the visuals
* Use the visualization tools in the spreadsheet program or Tableau, or RStudio if using R.
	*  RStudio is an IDE for R
* Tableau lets you pull in data from nearly any system and turn it into compelling visuals or actionable insights.
	* It offers built-in visual best practices, allowing for fast, easy, and useful data sharing or analyzation.
	* Includes an interactive dashboard to explore data interactively.

## Reflections
1. Data analysis is a logical and rational process meant to convey stories using data. However, intuition and experience also play a role, which is what separates an analyst apart from a machine.
2. The six steps to data analysis -- Ask, Prepare, Process, Analyze, Share, Act -- involve a certain mindset where one needs to focus on the data and the analysis process. Human bias is difficult to overcome, but the data analysis process is designed to counter it.
3. Data collection and analysis *needs* to be fair and ethical. If there is an under represented group, an analyst should collect more data from them than from the majority group to ensure bias doesn't interfere with the results. Ethically, an analyst needs to ensure that as few people as possible have access to the data, and that it is stored securely. 
4. Always ask yourself, what is the root cause? That's how an analyst finds answers, by digging deeper and deeper until there's nowhere left to go. It is crucial to continue asking why, even if it seems you've found the answer. When the question of why becomes no longer tenable, then you've reached the end.
5. A data analyst needs to be versatile. Using a combination of big picture and detail-oriented thinking is key to success, as is being able to switch between the two at will. Data can tell all kinds of stories, and some aren't as obvious to one way of thinking.
6. Data-driven decision-making requires data, as the name suggests, but also a gap analysis when in the Ask phase. This helps to make sure the data analysis asks the right questions to determine where the gaps are. Then, the data can show how to fill them.

## References
1. [[Data Analytics Certification Module 1 Week 1]]
2. [[Data Analytics Certification Module 1 Week 2]]
3. [[Data Analytics Certification Module 1 Week 3]]
4. [[Data Analytics Certification Module 1 Week 4]]
5. [[Data Analytics Certification Module 1 Week 5]]
6. [[Analytics references and resources]]