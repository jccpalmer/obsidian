# Data Analytics Module 4
Created: 2022-10-19 at 08:25

## Definitions
1. Data integrity: The accuracy, completeness, consistency, and trustworthiness of data throughout its lifecycle ^856fbb
2. Data replication: The process of storing data in multiple locations
3. Data transfer: The process of copying data from storage device to memory, or from one computer to another
4. Data manipulation: The process of changing data to make it more organized and easier to read ^5268c5
5. Sample size: A part of a population that is representative of the population
6. Sampling bias: A sample isn't representative of the population as a whole
7. Random sampling: A way of selecting a sample from a population so that every possible type of the sample has an equal chance of being chosen
8. Margin of error: The difference between the sample's results and the expected population results
9. Confidence level: The probability that your sample size accurately reflects the greater population
10. Confidence interval: The range of possible values that the population's result would be at the confidence level of the study
11. Statistical insignificance: The determination of whether the result 
12. Statistical power: The probability of getting meaningful results from a test
13. Hypothesis testing: A way to see if a survey or experiment has meaningful results
14. Estimated reponse rate: The percentage of people expected to complete the survey
15. Dirty data: Data that is incomplete, incorrect, or irrelevant to the problem you're trying to solve
16. Data engineers: Transform data into useful format for analysis and give it a reliable infrastructure
17. Data warehousing specialists: Develop processes and procedures to effectively store and organize data
18. Null: An indication that a value does not exist in a dataset
19. Field length: A tool for determining how many characters can be keyed into a field
20. Data validation: A tool for checking the accuracy and quality of data before adding or importing it
21. Validity: The concept of using data integrity principles to ensure measures conform to defined business rules or constraints
22. Accuracy: The degree of conformity of a measure to a standard or a true value
23. Completeness: The degree to which all required measures are known
24. Consistency: The degree to which a set of measures is equivalent across systems
25. Merger: An agreement that united two organization into a single new one
26. Data merging: The process of combining two or more datasets into a single dataset
27. Compatibility: How well two or more datasets are able to work together
28. Conditional formatting: A spreadsheet tool that changes how cells appear when values meet specific conditions
29. Remove duplicates: A tool that automatically searches for and eliminates duplicate entries from a spreadsheet
30. Text string: A group of characters within a cell, most often composed of letters, numbers, or both
31. Delimiter: A specified text separator, such as a comma
32. Syntax: A predetermined structure that includes all required information and its proper placement
33. SPLIT: A tool that divides text around a specified character and puts each fragment into a new, separate cell
34. CONCATENATE: A function that joins multiple text strings into a single string
35. COUNTIF: A function that returns the number of cells that match a specified value
36. LEN: A function that tells you the length of a text string by counting the number of characters if contains
37. LEFT: A function that gives you a set number of characters from the left side of a text string
38. RIGHT: A function that gives you a set number of characters from the right side of a text string
39. MID: A function that gives you a segment from the middle of a text string
40. TRIM: A function that removes leading, trailing, and repeated spaces in data
41. Pivot table: A data summarization tool that is used in data processing
42. VLOOKUP: A function that searches for a certain value in a column to return a corresponding piece of information; vertical lookup
43. Data mapping: The process of matching fields from one data source to another
44. Schema: A way of describing how something is organized
45. SQL: Structured Query Language that analysts use to work with databases, usually large datasets
46. CAST(): Can be used to convert anything from one data type to another
47. Float: A number that contains a decimal
48. Typecasting: Converting data from one type to another
49. CONCAT(): Adds strings together to create new text strings that can be used as unique keys
50. COALESCE(): Can be used to return non-null values in a list
51. Verification: A process to confirm that a data-cleaning effort was well-executed and the resulting data is accurate and reliable
52. Changelog: A file containing a chronologically ordered list of modifications made to a project
53. Find and replace: A tool that looks for a specified search term in a spreadshet and allows for replacing it with something else
54. COUNTA: A function that counts the total number of values within a specified range
55. CASE statement: Goes through one of more conditions and returns a value as soon as a condition is met
56. Documentation: The process of tracking changes, additions, deletions, and errors involved in the data-cleaning effort
57. IMPORTRANGE: Imports (pastes) data from one sheet to another and keeps it automatically updated
58. QUERY: Enables pseudo SQL (SQL-like) statements or a wizard to import the data
59. FILTER: Displays only the data that meets the specified conditions

## Concepts
- A strong analysis depends on the [[Data Analytics Module 4#^856fbb | data integrity]], while compromised data leads to weak analyses.
- [[Data Analytics Module 4#^5268c5 | Data manipulation]] is meant to make the data analysis process more efficient, but an error in the process can compromise that efficiency.
- Data integrity can fall under other threats, too, including:
	- Human error
	- Viruses
	- Malware
	- Hacking
	- System failures
- 

## Reflections

## References
1. 