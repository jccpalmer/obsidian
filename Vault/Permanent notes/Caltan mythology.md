# Caltan religion and mythology
Created: 2022-03-11 at 14:21

The Caltans hold differing religious views from the eastern Cardinal Church, which believes in a tripatite godhead: Death, Fate, and Destiny. The Caltans believe similarly, with three head gods: Midyir, God of Death; Dagdara, Goddess of Fate; and Brigit, Goddess of Eternity.

Under these four gods are seven demigods. There is a fourth goddess, Nuada, that the Order holds as its patron deity. She is not officially recognized as divine by the Caltan church, but it does not interefere with the Order's beliefs. [[Order mythology#Nuada | Nuada]] is called the Wielder of the White Light.

## References
1. [[How the Son of the Gobhaun Saor Sold the Sheepskin]]
2. [[The Earth-Shapers]]