# Data Analytics Certification Module 2 Week 4
Created: 2022-09-19 at 11:30

## Balance team and stakeholder needs
- Stakeholders: People that have invested time, interest, and resources into the projects analysts work on
- Turnover rate: The rate at which employees leave a company
- Project managers are in charge planning and executing a project
- Common stakeholder groups:
	- Executive team
		- The executive team provides strategic and operational leadership to the company. They set goals, develop strategy, and make sure that strategy is executed effectively. The executive team might include vice presidents, the chief marketing officer, and senior-level professionals who help plan and direct the company’s work. These stakeholders think about decisions at a very high level and they are looking for the headline news about your project first.  They are less interested in the details.
	- Customer-facing team
		- The customer-facing team includes anyone in an organization who has some level of interaction with customers and potential customers. Typically they compile information, set expectations, and communicate customer feedback to other parts of the internal organization. These stakeholders have their own objectives and may come to you with specific asks.
	- Data science team
- Tips for working effectively with stakeholders:
	- Tips for working effectively with stakeholders:
	- Discuss goals
	- Feel empowered to say "no"
	- Plan for the unexpected
	- Know your project
	- Start with words and visuals
	- Communicate often
- Questions to ask:
	- Who are the primary and secondary stakeholders?
	- Who is managing the data?
	- Where can you go for help?

## Communication is key
- Before you commincate, think about:
	- Who your audience is
	- What they already know
	- What they need to know
	- How you can communicate that effectively to them
- Learn as you go and ask questions
- Practice good writing habits
- Read emails aloud
- Set expectations early
- Flag problems early for stakeholders
- Set realistic expectations at every stage of the project
- Balance speed with accuracy
- Reframe the question, outline the problems and solutions
- Fastest answer is not the always the accurate one
- Data limitations
	- Incomplete or nonexistent
	- Misaligned
	- Dirty
- Telling a clear story:
	- Compare the same types of data
	- Visualize with care
	- Leave out needless graphs
	- Test for statistical significance
	- Pay attention to sample size

## Amazing teamwork
- Meeting dos
	- Come prepared
		- Bring what you need
		- Read the meeting agenda
		- Prepare notes and presentations
		- Be ready to answer questions
	- Be on time
	- Pay attention
	- Ask questions
- Meeting don'ts
	- Show up unprepared
	- Arrive late
	- Be distracted
	- Dominate the conversation
	- Talk over others
	- Distract people with unfocused discussion
- A conflict can pop up for a variety of reasons
- Use proper communication to resolve conflict