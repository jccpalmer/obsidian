# Data Analytics Certification Module 4 Week 5
Created: 2022-10-05 at 10:33

## The data analyst hiring process
- Job search differs for everyone
- Create a new or updated resume that is specific to what each company is looking for
- Having a master resume to tweak for each position is wise
- Be professional and personable when talking to recruiters
- Use LinkedIn to research hiring managers
- Make sure the offer is competitive before you sign
- There's usually room to negotiate salary and certain benefits
- Know your worth, but know that a company has placed a value on you for that role
- Give a two-week notice
- Keep the resume brief, usually to one page with 2-4 bullet points per description/position
	- Focus on the details you want to convey
- Summary is useful for information that may not be relevant to a data analyst (such as writing in my case) or if making a career transition (also my case)
	- 1-2 sentences that highlight strengths and contributions
	- Include positive words
		- "Entry-level data analytics professional; recently completed the Google Data Analytics Professional Certificate."
- In work experience descriptions, include work you did that relates to the position you're applying for
	- Skills, experience, and education needed for the job
- Highlight impact in role and impact role had on you
- Basic formula for resume:
	- Accomplished X
	- As measured by Y
	- By doing Z

## Understand the elements of a data analyst resume
- Convey that I am a clear communicator
- Summary
	- Eg: "Transitioning from a career in the journalism industry and seeking a full-time role in the field of data analytics."
- PAR statements
	- Problem
	- Action
	- Result
		- Eg: "Earned a moderately successful website more than 10 million organic visitors through strategic reporting, conclusive reviews, and solid journalistic ethics."
- Investigate CareerCon

## Highlighting experiences on resumes
- Transferable skills: Skills and qualities that can transfer from one job or industry to another
- Communication skills usually mean a person being able to communicate findings to non-technical people
- Highlight SQL, Excel, R, Tableau, and Python (when I get there)
- Consider adding soft skills like communication, adaptability, pattern recognition, and problem-solving

## Exploring areas of interest
- This certificate will be most applicable to junior or associate positions