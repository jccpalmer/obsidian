# Data Analytics Certification Module 2 Week 3
Created: 2022-08-15 at 11:20

## Working with spreadsheets
- Common math functions
	- Sum
	- Average
	- Count
	- Min
	- Max
- Tasks
	- Organize data
		- Pivot table
			- Sort and filter
	- Calculate your data
		- Formulae
- Analysts use functions and formulae to perform calculations in spreadsheets
- Spreadsheets and the data life cycle
	- **Plan** for the users who will work within a spreadsheet by developing organizational standards
		- Formatting cells
		- Highlighting headings
		- Color scheme
		- Way data is ordered
	- **Capture** data by the source by connecting spreadsheets to other data sources
		- Data will be automatically updated in the spreadsheet
	- **Manage** different kinds of data with a spreadsheet; allows for deciding who can access the data, how the info is shared, and how to keep the data safe and secure
		- Storing
		- Organizing
		- Filtering
		- Updating
	- **Analyze** data in a spreadsheet to help make better decisions
		- Common tools
			- Formulae to aggregate data or create reports
			- Pivot tables for clear visuals
	- **Archive** any spreadsheet not often used that might need to be referenced later with built-in tools
		- Useful for storing historical data before it gets updated
	- **Destroy** the spreadsheet when it is no longer needed, if there are better backup copies, or for legal/security reasons

## Formulae in spreadsheets
- Formula: A set of instructions that performs a specific calculation
- Operator: A symbol that names the type of operation or calculation ot be performed
- Start expression with =
- Operators:
	- + for additions
	- - for subtraction
	- * for multiplication
	- / for division
- Cell reference: A cell or range of cells in a worksheet that can be used in a formula
- Range: A collection of two or more cells
- Absolute references are marked with $
	- They will not change when you copy and paste the formula into a different cell
- Relative references will change anytime the formula is copied and pasted
- Use F4 to change between absolute and relative references
- Formulae and functions can be combined, such as with COUNTIF()
- Common errors:
	- #DIV/0!: A formula is trying to divide a value in a cell by 0 or by an empty cell
	- IFERROR(TK:TK, "N/A")
	- #ERROR! (Google Sheets only): A formula can't be interpreted as input (parsing error)
		- Often because of a missing delimiter
	- #N/A: Data in a formula can't be found by the spreadsheet
	- #NAME?: A formula or function name isn't understood
	- #NUM!: A formula or function calculation can't be performed as specified
	- #VALUE!: A general error that could indicate a problem with a formula or referenced cells
	- #REF!: A formula is referencing a cell that is no longer valid or has been deleted
- Best practices
	- Filter data to make your spreadsheet less complex and busy.
	- Use and freeze headers so you know what is in each column, even when scrolling.
	- When multiplying numbers, use an asterisk not an X.
	- Start every formula and function with an equal sign (=).
	- Whenever you use an open parenthesis, make sure there is a closed parenthesis on the other end to match.
	- Change the font to something easy to read.
	- Set the border colors to white so that you are working in a blank sheet.
	- Create a tab with just the raw data, and a separate tab with just the data you need.
- Conditional formatting can be used to highlight cells a different color based on their contents

## Functions in spreadsheets
- Function: A preset command that automatically performs a specific process or task using the data

## Save time with structured thinking
- Define problems before trying to solve them
- Problem domain: The specific area of analysis that encompasses every activity affecting or affected by the problem
- Think structurally
- Structured thinking: The process of recognizing the current problem or situation, organizing available information, revealing gaps and opportunities, and identifying the options
- Scope of work (SOW): An agreed-upon outline of the work you're going to perform on a project
	- Composed of:
		- Deliverables
			- What work is being done, and what things are being created as a result of this project? When the project is complete, what are you expected to deliver to the stakeholders? Be specific here. Will you collect data for this project? How much, or for how long?
		- Timeline
			- Your timeline will be closely tied to the milestones you create for your project. The timeline is a way of mapping expectations for how long each step of the process should take. The timeline should be specific enough to help all involved decide if a project is on schedule. When will the deliverables be completed? How long do you expect the project will take to complete? If all goes as planned, how long do you expect each component of the project will take? When can we expect to reach each milestone?
		- Milestones
			- This is closely related to your timeline. What are the major milestones for progress in your project? How do you know when a given part of the project is considered complete?
		- Reports
			- Good SOWs also set boundaries for how and when you’ll give status updates to stakeholders. How will you communicate progress with stakeholders and sponsors, and how often? Will progress be reported weekly? Monthly? When milestones are completed? What information will status reports contain?
	- Not to be confused with *statement of work*
- SOWs should also contain information specific to what is and isn’t considered part of the project.
- Figuring out what data means is just as important as collecting it
- There is no universal set of contextual interpretations
	- Context is personal
- Start with an accurate representation of the data and collect it in the most objective way possible

## References
1. [[Analytics references and resources#^eee04b | Spreadsheet Errors and Fixes]]