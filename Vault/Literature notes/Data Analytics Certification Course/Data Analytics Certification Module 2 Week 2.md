# Data Analytics Certification Module 2 Week 2
Created: 2022-08-12 at 07:34

## Understanding the power of data
* Data analysis can help us make more informed decisions
* Data-inspired decision-making: Exploration of different data sources to find out what they have in common
* Algorithm: A process or set of rules to be followed for a specific task
* Turn data into knowledge with context and information.
* Limitations:
	* Inconsistent access to needed data
	* Data is measured differently across programs
* When data is interpreted incorrectly, it can lead to huge losses.
* When data is used strategically, businesses can transform and grow their revenue.
* It is important for you to keep a data-driven mindset, ask lots of questions, experiment with many different possibilities, and use both logic and creativity along the way. You will then be prepared to interpret your data with the highest levels of care and accuracy.
* There is a key difference between making a decision with incomplete data versus a small dataset.
* Quantitative data: Specific and objective measures of numerical facts; quantity
	* The what
	* How many
	* How often
	* Things that can be measured
* Qualitative data: Subjective or explanatory measures of qualities and characteristics; quality
	* Things that can't be measured with numerical data
	* Answer why questions
* Quantitative data can be visualized with charts and graphs; qualtitative data can give a higher level understanding of why the numbers are the way they are.
* Both types of data help to make the right changes and improve the business.
* Quantitative data tools
	* Focus groups
	* Social media text analysis
	* In-person interviews
* Qualitative data tools
	* Structured interviews
	* Surveys
	* Polls

## Follow the evidence
* Report: A static collection of data given to stakeholders periodically
* Dashboard: A monitor for live, incoming data
* Reports
	* Pros
		* High-level historical data
		* Easy to design and use
		* Pre-cleaned and sorted data
	* Cons
		* Continual maintenance
		* Less visually appealing
		* Static
* Dashboards
	* Pros
		* Dynamic, automatic, and interactive
		* More stakeholder access
		* Low maintenance
		* More visually appealing
	* Cons
		* Labor-intensive design
		* Can be confusing and less efficient
		* Potentially uncleaned data
* Pivot table: A data summarization tool that is used to summarize, sort, reorganize, group, count, total, or average data stored in a database or spreadsheet
* Metric: Single, quantifiable type of data that can be used for measurement
	* Usually involve simple math
* Choosing the right metric is key.
	* Data contains a lot of raw details about the problem being explored.
* Metric goal: A measurable goal set by a company and evaluated using metrics
* Dashboard benefits
	* Analysts
		* Centralization: Sharing a single source of data with all stakeholders
		* Visualizaton: Showing and updating live, incoming data in real time
		* Insightfulness: Pulling relevant info from different datasets
		* Customization: Creating custom views dedicated to a specific person, project, or presentation of the data
	* Stakeholders
		* Centralization: Working with a comprehensive view of data, initiatives, objectives, projects, processes, and more
		* Visualization: Spotting changing trends and patters quicker
		* Insightfulness: Understanding the story behind the numbers to keep track of goals and make data-driven decisions
		* Customization: Drilling down to more specific areas of specialized interest or concern
	* Creating a dashboard:
		1. Identify the stakeholders who need to see the data and how they will use it
			1. Ask effective questions
		2. Design the dashboard
			1. Use a clear header to label the information
			2. Add short text descriptions to each visualization
			3. Show the most important information at the top
		3. Create mock-ups if desired
		4. Select the visualizations you will use on the dashboard
			1. Line/bar graphs: Change in values over time
			2. Pie/donut: How each part contributes to the whole
		5. Create filters as needed
			1. Filters show certain while hiding the rest of the data in a dashboard


## Connecting the data dots
* Mathematical thinking: Looking at a problem and logically breaking it down step by step to see the relationship and patterns in the data
	* Helps to determine best tools to use for analysis
* Deciding on a tool
	* Size of dataset
		* Small data = use spreadsheets
			* Describes a dataset made up of specific metrics of a short, well-defined time period
			* Usually organized and analyzed in spreadsheets
			* Day-to-day decisions
			* Likely to be used by small and midsize businesses
			* Simple to collect, store, manage, sort, and visually represent
			* Usually already a manageable size for analysis
		* Big data = databases/SQL
			* Describes large, less-specific datasets that cover a long time period
			* Usually kept in a database and queried
			* Likely to be used by large organizations
			* Takes a lot of effor to collect, store, manage, sort, and visually represent
			* Usually needs to be broken into smaller pieces in order to be organized and analyzed effectively decision making
* Bed occupancy rate = Total number of inpatient days for a given period x 100 / Available beds X number of days in the period
* Big data challenges:
	*  A lot of organizations deal with data overload and way too much unimportant or irrelevant information.
	* Important data can be hidden deep down with all of the non-important data, which makes it harder to find and use. This can lead to slower and more inefficient decision-making time frames.
	* The data you need isn’t always easily accessible.
	* Current technology tools and solutions still struggle to provide measurable and reportable data. This can lead to unfair algorithmic bias.
	* There are gaps in many big data business solutions.
- Big data benefits:
	- When large amounts of data can be stored and analyzed, it can help companies identify more efficient ways of doing business and save a lot of time and money.
	- Big data helps organizations spot the trends of customer buying patterns and satisfaction levels, which can help them create new products and solutions that will make customers happy.
	- By analyzing big data, businesses get a much better understanding of current market conditions, which can help them stay ahead of the competition.
	- As in our earlier social media example, big data helps companies keep track of their online presence—especially feedback, both good and bad, from customers. This gives them the information they need to improve and protect their brand.
- Three V words for big data
	- Volume: The amount of data
	- Variety: The different kinds of data
	- Velocity: How fast the data can be processed
	- Veracity: The quality and reliability of the data

## References
1. [[Analytics references and resources#^502fc1 | How 1 retailer's data strategy powers seamless customer experiences]]
2. [[Analytics references and resources#^345e73 | How one of the world's biggest markets ripped up its playbook and learned to anticipate intent]]
3. [[Analytics references and resources#^d01320 | Designing Compelling Dashboards]]
4. [[Analytics references and resources#^39dd79 | Real-World Examples of Business Intelligence Dashboards]]
5. [[Analytics references and resources#^9e0b7f | Requirements Gathering Worksheet]]
6. [[Analytics references and resources#^f7d0dd | Filter Actions]]