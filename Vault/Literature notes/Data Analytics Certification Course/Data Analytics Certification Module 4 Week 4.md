# Data Analytics Certification Module 4 Week 4
Created: 2022-10-03 at 20:42

## Manually cleaning data
- Verification: A process to confirm that a data-cleaning effort was well-executed and the resulting data is accurate and reliable
- Verifying clean data means rechecking the cleanliness of the dataset, doing manual cleanups if needed, and taking moments to sit back and think about the original purpose of the project
- Without verification, the data analysis cannot be trusted
- Reports are an effective means of displaying transparency during the analytics process
- Changelog: A file containing a chronologically ordered list of modifications made to a project
- Changelogs usually contain the version, the date, and a list of the improved/removed features
- First step in the verification process is going back to the original unclean dataset to compare to the cleaned dataset
	- Try to identify any common problems
- See the big picture when verifying data-cleaning:
	- Consider the business problem
	- Consider the goal
	- Consider the data
- Step back and ask, "Do the numbers make sense?"
- Find and replace: A tool that looks for a specified search term in a spreadshet and allows for replacing it with something else
- COUNTA: A function that counts the total number of values within a specified range
- CASE statement: Goes through one of more conditions and returns a value as soon as a condition is met
```
	SELECT
		columnX,
		CASE
			WHEN columnY = 'EXAMPLE_MISPELLED' THEN 'EXAMPLE_SPELLED'
			ELSE columnY
			END AS columnZ
	FROM
		dataset.table
```
- Most common problems:
	- Sources of errors: Did you use the right tools and functions to find the source of the errors in your dataset?
	- Null data: Did you search for NULLs using conditional formatting and filters?
	- Misspelled words: Did you locate all misspellings?
	- Mistyped numbers: Did you double-check that your numeric data has been entered correctly?
	- Extra spaces and characters: Did you remove any extra spaces or characters using the TRIM function?
	- Duplicates: Did you remove duplicates in spreadsheets using the Remove Duplicates function or DISTINCT in SQL?
	- Mismatched data types: Did you check that numeric, date, and string data are typecast correctly?
	- Messy (inconsistent) strings: Did you make sure that all of your strings are consistent and meaningful?
	- Messy (inconsistent) date formats: Did you format the dates consistently throughout your dataset?
	- Misleading variable labels (columns): Did you name your columns meaningfully?
	- Truncated data**:** Did you check for truncated or missing data that needs correction?
	- Business Logic: Did you check that the data makes sense given your knowledge of the business?

## Documenting results and the cleaning process
- Documentation: The process of tracking changes, additions, deletions, and errors involved in the data-cleaning effort
- A record of how data evolved is important because:
	- Recover data-cleaning errors
	- Inform other users of changes
	- Determine the quality of the data
		- The first two assume that the data errors aren't fixable
- Changelogs are helpful for determining and explaining why the changes were made
- Typical changelog info:
	- Data, file, formula, query, or any other component that changed
	- Description of what changed
	- Date of the change
	- Person who made the change
	- Person who approved the change
	- Version number
	- Reason for the change
- Best changelog practices:
	- Changelogs are for humans, so write legibly
	- Every version should have its own entry
	- Each change should have its own line
	- Group the same types of changes
	- Versions should be ordered chronologically with the latest
	- The release date of each version should be noted
- Types of changes:
	- Added: New features introduced
	- Changed: Changes in existing functionality
	- Deprecated: Features about to be removed
	- Removed: Features that have been removed
	- Fixed: Bug fixes
	- Security: Lowering vulnerabilities
- Reporting on documentation keys in stakeholders to what's going on with the data
- Common data errors
	- Human error in data entry
	- Flawed processes
	- System issues
- Data cleaning can shine a light on the nature and severity or error-generating processes
- IMPORTRANGE: Imports (pastes) data from one sheet to another and keeps it automatically updated
- QUERY: Enables pseudo SQL (SQL-like) statements or a wizard to import the data
- FILTER: Displays only the data that meets the specified conditions

## References
1. [[Analytics references and resources]]