# Data Analytics Certification Module 1Week 2
Created: 2022-08-05 at 08:01

## Key data analyst skills
* Analytical skills are qualities and characteristics associated with solving problems using facts
* Five essential aspects to analytical skills
	* Curiosity: Wanting to learn something, seeking out new challenges and experiences
	* Understanding: Context is crucial
		* Context: The condition in which something exists or happens
	* Technical mindset: The ability to break down the things into smaller steps or pieces and work with them in an orderly and logical way
	* Data design: How one organizes information, such as with a database or phone contacts
	* Data strategy: The management of the people (the ones who know how to use the right data to find solutions), processes (or ensuring that the path to that solution is clear and accessible), and tools used in data analysis (the right technology is being used for the job)
* Analytical thinking involves identifying and defining a problem before solving it
	* Its five key aspects are visualization, strategy, problem-orientation, correlation, and big-picture and detail-oriented thinking
		* Visualization: The graphical representation of information
		* Strategizing: Helps see the goals and possible processes with understanding the data; improves the quality and usefulness of the data collected
	* To solve a problem, we use data in an organized, step-by-step manner
* Data analysts use a problem-oriented approach to identify, describe, and solve problems
	* They could identify correlations between two or more pieces of data
	* Ask a lot of questions
* Correlation does not equal causation! 
	* But it could indicate a relationship
* Big picture thinking involves zooming out and seeing possibilities and opportunities
* Detail-oriented thinking is about figuring out all aspects that help with plan execution
* All sorts of business problems benefit from both big picture and detail-oriented thinking
* Solutions are seldom obvious
* One might be naturally analytical, but thinking creatively and criticlally can also be learned
	* Versatility is key to data analysis
	* The more ways one can think, the easier it is to think outside the box and come up with fresh ideas
* What is the root cause of the problem?
* Root cause: The reason why a problem occurs
* Five Whys
	* Ask "why?" five times to reveal the root cause
	* Fifth answer should give useful, and sometimes surprising, insights
* Where are the gaps in the process?
* Gap analysis: Allows the examination and evaluation of how a process works currently to get to a long-term future goal
	* Businesses conduct gap analysis all the time
	* Understanding where you are now and where you want to be later
* What did we not consider before?
	* What information or procedure might be missing from a process
* In business, data-driven decision-making can improve results in different ways
* Data analysts can tap into data to gain valuable insights, verify their theories or assumptions, better understand opportunities and challenges, support an objective, and make a plan
* Data-driven decision making increases the confidence in the decision and the ability to address business challenges
	* It helps an analyst to become more proactive when an opportunity presents itself, and it saves time and effort when working toward a goal
	* Reminder: data-driven decision making involves using facts to guide business strategy
* Data analysts use context to make predictions, research answers, and draw conclusions
* Data analysts use a technical mindset to seek out the facts, put them to work through analysis, and using the insights to make informed decisions
* Data design has a strong connection to data-driven decision making
	* Designing data such that it is organized in a logical way makes it easier for other analysts to access, understand, and make the most of said data
	* Not just applicable to databases
* A data strategy incorporates the people, processes, and tools used to solve a problem
	* It offers a high-level view of the path an analyst should take to achieve their goals
* It is important the ensure that specific procedures are in place so that everyone is on board and on the same page
* Quartile: Divides data points into four equal parts
* Nonprofit: An organization dedicated to advancing a social cause or advocating for a particular effort

## References
1. 