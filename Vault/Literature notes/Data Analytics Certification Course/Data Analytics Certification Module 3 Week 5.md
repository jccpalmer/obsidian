# Data Analytics Certification Module 3 Week 5
Created: 2022-09-26 at 16:20

## Create or enhance your online presence
- A professional online presence can:
	- Help potential employers find you
	- Make connections with other analysts
	- Learn and share findings
	- Participate in community events
- Connection (LinkedIn): A person known and trusted on a personal or professional basis
- Check privacy settings on social media accounts
- Posts should be SFW

## Build a data analytics network
- Networking: Professional relationship building
- Search for public meetups in your area
- Podcasts to check out:
	- Partially Derivative
	- O'Reilly Data Show
- Hashtags to follow:
	- #dataanalyst
	- #dataanalytics
	- #datascience
- Check out "data science" on Meetup
- Mentor: A professional who shares their knowledge, skills, and experience to help you develop and grow
- Mentor websites to look into:
	- SCORE.org
	- MicroMentor.org
	- Mentorship
- Sponsor: A professional advocate who's committed to moving a sponsee's career forward within an organization
- A mentor helps you *skill* up, a sponsor helps you *move* up

## References
1. [[Analytics references and resources]]