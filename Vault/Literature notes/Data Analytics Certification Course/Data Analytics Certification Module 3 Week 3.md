# Data Analytics Certification Module 3 Week 3
Created: 2022-09-24 at 22:33

## Working with databases
- Database: A collection of data stored in a computer system
- Meta means referring back to itself
- Metadata: Data about data
- Metadata tells you where the data comes from, when and how it was created, and what it's all about
- Relational database: A database that contains a series of related tables that can be connected via their relationships
- To have a relationship, one or more of the same fields must exists inside both tables
- Primary key: An identifier that references a column in which each value is unique
- Primary keys should be unique, and cannot be null or blank
- Foreign key: A field within a table that is a primary key in another table
- Foreign keys are how one table can be connected to another
- One primary key and multiple foreign keys
- Primary key:
	- Used to ensure data in a specific column is unique
	- Uniquely identifies a record in a relational database table
	- Only one primary key is allowed in a table
	- Cannot contain null or blank values
- Foreign key:
	- A column or group or columns in a relational database table that provides a link between the data in two tables
	- Refers to the field in a table that's the primary key of another table
	- More than one foreign key is allowed to exist in a table
- Normalization: A process of organizing data in a relational database
- Tables in a relational database are connected by the fields they have in common
- Composite key: A primary key constructed using multiple columns of a table
- Before beginning an analysis, it's important to inspect the data to determine if it contains the specific information needed to answer the stakeholders' questions
	- The data is not there
	- The data is insufficient
	- The data is incorrect
- Inspecting the dataset will help pinpoint what questions are answerable and what data is still missing

## Managing data with metadata
- Metadata is used in database management to help data analysts interpret the contents of the data within the database
- Three common types of metadata:
	- Descriptive
	- Structural
	- Administrative
- Descriptive metadata: Metadata that describes a piece of data and can be used to identify it at a later point in time
- Structural metadata: Metadata that indicates how a piece of data is organized and whether it is part of one, or more than one, data collection
	- Keeps track of the relationship between two things
- Administrative metadata: Metadata that indicates the technical source of a digital asset
- Elements of metadata:
	- Title and description: What is the name of the file or website you are examining? What type of content does it contain?
	- Tags and categories: What is the general overview of the data that you have? Is the data indexed or described in a specific way?
	- Who created it and when: Where did the data come from, and when was it created? Is it recent, or has it existed for a long time?
	- Who last modified it and when: Were any changes made to the data?  If yes, were the modifications recent?
	- Who can access or update it: Is this dataset public? Are special permissions needed to customize or modify the dataset?
- Examples:
	- Photo info
	- Email info
	- Spreadsheet/document info
	- Website info
	- File info
	- Book info
- Benefits of using metadata:
	- Metadata creates a single source of truth by keeping things consistent and uniform
		- After all, data that's uniform can be organized, classified, stored, accessed, and used effectively. Plus, when a database is consistent, it's so much easier to discover relationships between the data inside it and the data elsewhere
	- Metadata also makes data more reliable by making sure it's accurate, precise, relevant, and timely
- Metadata repository: A database specifically created to store metadata
	- Can be phyiscal or virtual
- Metadata repos describe where metadata comes from, keep it in an accessible form so it can be used quickly and easily, and keep it in a common structure for everyone who may need to use it
- Metadata repos make it easier and faster to bring together multiple sources for data analysis, confirm how or when data was collected, and verify that data from an outside source is being used properly
- Metadata repositories:
	- Describe the state and location of the metadata
	- Describe the structures of the tables inside
	- Describe how the data flows through the repo
	- Keep track of who accesses the metadata and when
- Metadata is stored in a single, central location, and gives the company standardized information about all of its data
	- Metadata contains information about where each system is located and where the datasets are located within those systems
	- Metadata describes how all of the data is connected between the various systems
- Data governance: A process to ensure the formal management of a company's data assets
- Data governance is about the roles and responsibilities of the people who work with the metadata every day

## Accessing different data sources
- Internal data (primary data) is generated within a company
- External data (secondary data) can come from a variety of sources
- IMPORTRANGE (Google Sheets) allows for importing data from other spreadsheets
	- Excel allows for importing spreadsheets directly (Data > Get Data > From File > From Workbook > Select worksheet to import > Load or Transform Data)
- Data can be imported from CSV files and HTML tables
	- Sheets uses the IMPORTHTML function
	- Excel uses the From Web option
- CSV: Comma-separated values; a file saved in a table format
	- Does not contain formatting

## Sorting and filtering
- Sorting data: Arranging data into a meaningful order to make it easier to understand, analyze, and visualize
- Filtering: Showing only the data that meets a specific criteria while hiding the rest

## Working with large datasets in SQL
- BigQuery account types
	- Sandbox
		- No charge
		- 12 projects at a time
		- Cannot insert new records into a database
		- Cannot update field values of existing records
	- Free trial
		- $300 credit during the first 90 days
		- Select to upgrade to a paid account
		- Never be auto charged after trial

## References
1. [[Analytics references and resources]]