# Data Analytics Certification Module 3 Week 2
Created: 2022-09-24 at 20:26

## Unbiased and objective data
- Bias: A preference in favor of or against a person, group, or thing
	- Conscious or unconscious
- Data bias: A type of error that systematically skews results in a certain direction
- Think about bias and fairness from the moment you start collecting data to the time where you present your findings
- Our biases can created biased data
- Sampling bias: When a sample isn't representative of the population as a whole
- Unbiased sampling: When a sample is representative of the population being measured
- Observer bias: The tendency for different people to observe things differently
- Interpretation bias: The tendency to always interpret ambiguous situations in a positive or negative way
- Confirmation bias: The tendency to search for or interpret information in a way that confirms pre-existing beliefs

## Explore data credibility
- Choosing quality data sources, remember ROCCC:
	- Reliable
	- Original
	- Comprehensive
	- Current
	- Cited
- Go with vetted public data sets, academic papers, financial data, and governmental data
- Bad data breaks ROCCC, could be wrong, or filled with error
- Bad cannot be trusted because it is inaccurate, incomplete, or biased
- Bad data's original source cannot be located
- Bad data sources are missing important information needed to answer the question or find the solution
- Bad data sources are out of date and irrelevant
- Bad data is not cited or sourced properly
- Every good solution is found by avoiding bad data

## Data ethics and privacy
- Ethics: Well-founded standards of right and wrong that prescrive what humans ought to do, usually in terms of rights, obligations, benefits to society, fairness, or specific virtues
- Data ethics: Well-founded standards of right and wrong that dictate how data is collected, shared, and used
- GDPR: General Data Protection Regulation of the EU
- Aspects of data ethics:
	- Ownership: Individuals own the raw data they provide and they have primary control over its usage, how it's processed, and how it's shared
	- Transaction transparency: All data processing activities and algorithms should be completely explainable and understood by the individual who provides their data
	- Consent: An individual's right to know explicit details about how and why their data will be used before agreeing to provide it
	- Currency: Individuals should be aware of financial transactions resulting from the use of their personal data and the scale of these transactions
	- Privacy: Preserving a data subject's information and activity any time a data transaction occurs
		- Protection from unauthorized access to our private data
		- Freedom from inappropriate use of our data
		- The right to inspect, update, or correct our data
		- Ability to give consent to use our data
		- Legal right to access data
	- Openness: Free access, usage, and sharing of data
- Personally Identifiable Information (PII): Information that can be used by itself or with other data to track down a person's identity
- Data anonymization: The process of protecting people's prviate or sensitive data by eliminating that kind of information
- De-identification: A process used to wipe data clean of all PII

## Understanding open data
- Openness (open data): Free access, usage, and sharing of data
- Availability and access: Open data must be available as a whole
- Reuse and Redistribution: Open data must be available under terms that allow reuse and redistribution
- Universal Participation: Everyone must be able to use, reuse, and redistribute the data
- Open data benefits from credible databases being more widely available
- Data interoperability: The ability of data systems and services to openly connect and share data
- Interoperability requires a lot of cooperation

## References
1. [[Analytics references and resources]]