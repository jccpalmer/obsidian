# Data Analytics Certification Module 2 Week 1
Created: 2022-08-09 at 12:41

## Data in action
### Problem-solving and effective questioning
* Structured thinking: The process of recognizing the current problem or situation, organizing the available information, revealing gaps and opportunities, and identifying the options

### Take action with data
* Ask
	* It’s impossible to solve a problem if you don’t know what it is. These are some things to consider:
		* Define the problem you’re trying to solve
		* Make sure you fully understand the stakeholder’s expectations
		* Focus on the actual problem and avoid any distractions
		* Collaborate with stakeholders and keep an open line of communication
		* Take a step back and see the whole situation in context
	* Questions to ask:
		* What are my stakeholders saying their problems are?
		* Now that I've identified the issues, how can I help the stakeholders resolve their questions?
* Prepare
	* You will decide what data you need to collect in order to answer your questions and how to organize it so that it is useful. You might use your business task to decide:
		* What metrics to measure
		* Locate data in your database
		* Create security measures to protect that data
	* Questions to ask:
		* What do I need to figure out how to solve this problem?
		* What research do I need to do?
* Process
	* Clean data is the best data and you will need to clean up your data to get rid of any possible errors, inaccuracies, or inconsistencies. This might mean:
		* Using spreadsheet functions to find incorrectly entered data
		* Using SQL functions to check for extra spaces
		* Removing repeated entries
		* Checking as much as possible for bias in the data
	* Questions to ask:
		* What data errors or inaccuracies might get in my way of getting the best possible answer to the problem I am trying to solve?
		* How can I clean my data so the information I have is more consistent?
* Analyze
	* You will want to think analytically about your data. At this stage, you might sort and format your data to make it easier to:
		* Perform calculations
		* Combine data from multiple sources
		* Create tables with your results
	* Questions to ask:
		* What story is my data telling me?
		* How will my data help me solve this problem?
		* Who needs my company's product or service? What type of person is most likely to use it?
* Share
	* Everyone shares their results differently so be sure to summarize your results with clear and enticing visuals of your analysis using data via tools like graphs or dashboards. This is your chance to show the stakeholders you have solved their problem and how you got there. Sharing will certainly help your team:
		* Make better decisions
		* Make more informed decisions
		* Lead to stronger outcomes
		* Successfully communicate your findings
	* Questions to ask:
		* How can I make what I present to the stakeholders engaging and easy to understand?
		* What would help me understand this if I were the listener?
* Act
	* Now it’s time to act on your data. You will take everything you have learned from your data analysis and put it to use. This could mean providing your stakeholders with recommendations based on your findings so they can make data-driven decisions.
		* Questions to ask:
			* How can I use the feedback I received during the Share phase to actually meet the stakeholder’s needs and expectations?

### Solve problems with data
* Six common types of problems
	1. Making predictions
		* Using data to make an informed decision about how things may be in the future
	2. Categorizing things
		* Assigning information to different groups of clusters based on common features
	3. Spotting something unusual
		* Identifying data that is different from the norm
	4. Identifying themes
		* Grouping categorized information into broader concepts; takes categories a step further by grouping them into broader themes
	5. Discovering connections
		* Finding similar challenges faced by different entities and combining data and insights to address them
	6. Finding patterns
		* Using historical data to understand what happened in the past and is therefore likely to happen again

### Craft effective questions
* Leading question: Leading a person to answer a certain way
* Close-ended question: The only answers are "yes" and "no"
* Vague question: A question that lacks context or meaning
* SMART
	* Specific
		* Simple, significant, and focused on a single topic or a few closely-related ideas
	* Measurable
		* Can be quantified and assessed
	* Action-oriented
		* Encourage change
	* Relevant
		* Matter, are important, and have significance to the problem
	* Time-bound
		* Specify the time to be studied
* Fairness: Ensuring questions don't create or reinforce bias
* SMART questions need to be fair
* Leading questions and questions that make assumptions are also unfair
* Questions need to clear with wording that anyone can understand
* Unfair questions can make the job more difficult thanks to unreliable feedback and missed opportunities for insight

## References
1. 