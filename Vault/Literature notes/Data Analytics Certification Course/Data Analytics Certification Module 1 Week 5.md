# Data Analytics Certification Module 1 Week 5
Created: 2022-08-05 at 13:14

## Data analyst job opportunities
* Many businesses require data analysts
* Reliable application of data requires a reliable data analyst
* Small businesses are also starting to take advantage of data-driven insights to improve their operations and make better decisions
	* Analyze buying habits, create better social media outreaches, gather attendance data

## The importance of fair business decisions
* Business task: The question or problem that data analysis answers for a business
* Issue: A topic or subject to investigate
* Question: A statement designed to discover information
* Problem: An obstacle or complication that needs to be worked out
* Data-driven decision making is when facts discovered through data analysis are used to guide business strategy
* Data analysis provides the information needed to find the best possible solution to a problem
* Data provides a complete picture of the problem and its causes
	* This allows for the discovery of new and surprising solution that wouldn't have been possible to see before
* Data is data
* Data analysts have an obligation to ensure that their analysis is fair and ethical
* Fairness: Ensuring that the analysis does not create or reinforce bias
	* No cherry-picking
* Analysts need to create systems that are fair and inclusive
* Sometimes conclusions based on data can be both true *and* unfair
	* E.g. an ethical data analyst can look at the data gathered and conclude that the company culture is preventing employees from succeeding
	* Cherrypicking data can lead to erroneous conclusions, especially if someone using inductive reasoning to reach said conclusions
* When it comes to data analytics, it's not just about minimizing harm but the concept of beneficence
	* Beneficence: the conept of keeping the welfare of the subjects in mind; the quality or state of doing or producing good
* People should be able to consent to, or revoke consent of, collection of their data
* Analysts should empower people to have control over their data

## Exploring your next job
* Every industry has specific data needs
	* Finance, telecom, and tech all use data differently and thus need analysts who have different skills
* The industry's needs determine the tasks, questions, and job search
* Consider personal interests to figure out which data analytics industry to specialize in
* Think about how you can use your skills in a job
* Business analyst — analyzes data to help businesses improve processes, products, or services
* Data analytics consultant — analyzes the systems and models for using data
* Data engineer — prepares and integrates data from different sources for analytical use
* Data scientist — uses expert skills in technology and social science to find trends through data analysis
* Data specialist — organizes or converts data for use in databases or software systems
* Operations analyst — analyzes data to assess the performance of business operations and workflows
* Marketing analyst — analyzes market conditions to assess the potential sales of products and services
* HR/payroll analyst — analyzes payroll data for inefficiencies and errors
* Financial analyst — analyzes financial status by collecting, monitoring, and reviewing data
* Risk analyst — analyzes financial documents, economic conditions, and client data to help companies determine the level of risk involved in making a particular business decision
* Healthcare analyst — analyzes medical data to improve the business aspect of hospitals and medical facilities
* Increase professional network
	* Online footprint
	* LinkedIn
	* Meetups with other analysts and data scientists
* Prepare specific questions for interviewer
* In interviews, expect:
	* Case study
	* Sample data set
	* Business problem
	* Analysis
	* Solution
	* Present findings
		* Make sure that the answer relates back to data
		* Sometimes there is no right answer and interviewers are looking for an applicant's thought process

## References
1. [[Analytics references and resources#^4e7d7f | Beyond the Numbers]]