# Data Analytics Certification Module 4 Week 3
Created: 2022-10-02 at 12:13

## Using SQL to clean data
- SQL: Structured Query Language that analysts use to work with databases, usually large datasets
- SQL can process massive datasets quickly, whereas spreadsheets cannot
- Spreadsheets can:
	- Process smaller datasets
	- Enter data manually
	- Create graphs and visualizations in the same program
	- Use built-in spell check and other functions
	- Work best when working solo on a project
- Databases can:
	- Process larger datasets
	- Access tables across a database
	- Prepare data for further analysis in another software
	- Utilize fast and powerful functionality
	- Be great for collaborative work and tracking queries run by all users
- There are tools that can be used in both spreadsheets and SQL to achieve similar results
- SQL supports arithmetic, formulae, and data joining, just like spreadsheets
- SQL can combine COUNT and WHERE functions
- SQL and spreadsheet differences
	- Spreadsheets
		- Generated with a program
		- Spreadsheets have access to the data you input
		- Stored locally
		- Small datasets
		- Working independently
		- Built-in functionalities
	- SQL
		- A language used to interact with database programs
		- Can pull information from different sources in the database
		- Stored across a database
		- Larger datasets
		- Tracks changes across team
		- Useful across multiple programs

## Learn basic SQL queries
- SELECT
- FROM
- INSERT INTO
```
	INSERT INTO dataset.table
		(column1, column2,... columnX)
	VALUES
		(column1, column2,... columnX)
```
- UPDATE
```
	UPDATE dataset.table
	SET old_value = new_value
	WHERE table = columnX
```
- CREATE TABLE IF NOT EXISTS
	- Creates a new table to store the queried information
- DROP TABLE IF EXISTS
	- Deletes table (use sparingly for housekeeping, not with original data)
- DISTINCT
	- Including SELECT statement
	```
	SELECT
		DISTINCT columnX
	FROM dataset.table
	```
- LENGTH
```
	SELECT
		LENGTH(columnX) AS name
	FROM
		dataset.table
```

```
	SELECT
		columnX
	FROM
		dataset.table
	WHERE
		LENGTH(columnX) >/</= X
```
- SUBSTR()
```
	SELECT
		columnX
	FROM
		dataset.table
	WHERE
		SUBSTR(columnX,letter_to_start,letters_to_pull) = X
```
- TRIM()
```
	SELECT
		columnX
	FROM
		dataset.table
	WHERE
		TRIM(columnY) = 'X'
```

## Transforming data
- CAST(): Can be used to convert anything from one data type to another
```
	SELECT
		CAST(columnX AS DATATYPE64)
	FROM
		dataset.table
	WHERE
		CAST(columnX AS DATATYPE64)
```
- Float: A number that contains a decimal
- Typecasting: Converting data from one type to another
- CONCAT(): Adds strings together to create new text strings that can be used as unique keys
```
	SELECT
		CONCAT(column1,column2) AS new_string_name
	FROM
		dataset.table
```
- COALESCE(): Can be used to return non-null values in a list
```
	SELECT
		COALESCE(column1, column2) AS new_field_name
	FROM
		dataset.table
```
- Nulls are missing values

## References
1. [[Analytics references and resources]]