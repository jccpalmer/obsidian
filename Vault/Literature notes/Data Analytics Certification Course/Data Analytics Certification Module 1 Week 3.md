# Data Analytics Certification Module 1 Week 3
Created: 2022-08-05 at 08:01

## Learning about data phases and tools
* Data analysis tools include:
	* Spreadsheets
	* Databases
	* Query languages (SQL)
	* Visualization software (Tableau)
* The stages in the data life cycle help to understand the individual phases that data goes through before starting the project analysis
* Data life cycle:
	* Plan
		* What kind of data is needed?
		* How will the data be managed throughout its life cycle?
		* Who will be responsible for the data?
		* What are the optimal outcomes?
		* Happens before an analysis project
	* Capture
		* Collect the data from a variety of sources and bring it into the organization
	* Manage
		* How the data is cared for
		* How and where the data is stored
		* The tools used to keep it safe and secure
		* The actions taken to make sure it's maintained properly
		* Important to data cleansing
	* Analyze
		* The data is used to solve problems, make great decisions, support business goals
	* Archive
		* Store the data in a safe place where it is still available, but may not be used again
	* Destroy
		* Important for protecting a company's private information, as well as private data about customers
	* Database: A collection of information stored inside a computer system
	* Data analysis isn't a life cycle, it's the process of analyzing data
	* Ask
		* Define the problem to be solved
			* Look at the current state and identify how it's different from the ideal state
		* Understand stakeholder expectations
			* Determine who the stakeholders are
			* Communicating with stakeholders is key with staying on task
		* Stakeholders: People who have invested time and resourced into a project and are interested in the outcome
		* Ask all of the right questions at the beginning of the engagement so that you can better understand what your leaders and stakeholders need from this analysis
			* “What is the problem that we are trying to solve,” “What is the purpose of this analysis,” and “What are we hoping to learn from it?”
	* Prepare
		* Collect and store data for the analysis project
		* Data and results should be objective and unbiased
		* We need to think about what type of data we need to answer those key questions
	* Process
		* FInd and eliminate any errors or inaccuracies in the data
			* Cleaning, removing outliers, transforming into a more useful format, combining two or more datasets to make information complete
		* Check data the make sure it's complete and correct
	* Analyze
		* Using tools to transform and organize the information in order to draw conclusions, make predictions, and drive informed decision making
		* Spreadsheets and SQL
		* Data analysts are trained to look for patterns, but the data are not our story to tell; this is the point where we must take a step back and let the data speak for themselves
			* We might have a sneaking suspicion as to what the data will tell us
	* Share
		* How analysts interpret results and share them with others
		* Visualization is an analyst's best friend
		* Slideshows and prepared to answer questions
	* Act
		* Where the business acts on the analyst's analysis
	* The most common tools you will see analysts use are spreadsheets, query languages, and visualization tools
		* These tools allow analysts to be more focused on maximizing everything the former could do, streamlining their reporting, and just making their work simpler
		* Popular spreadsheet programs are Excel and Sheets
			* Spreadsheets store, organize, and sort data
			* Data usefulness depends on how it's structured
			* Spreadsheets let you Iientify patterns and piece the data together in a way that works for each specific data project
				* As well as create excellent data visualizations, like graphs and charts
	* Formula: A set of instructions that performs a specific calculation using the data in a spreadsheet
		* They can do basic things, such as add, subtract, multiply, and divide, but they do not stop there
	* Function: A preset command that automatically performs a process or task using the data in a spreadsheet
	* Query language: A computer programming language that allows for the retrieval and manipulation of data from a database
		* Structured Query Language (SQL) is a data analysis tool
		* Common SQL tools include MySQL, MS SQL Server, and Big Query
		* Allow analysts to isolate specific information from a database; make it easier for them to learn and understand the requests made to databases; allow them to select, create, add, or download data from a database for analysis
	* Tableau is a popular visualization tool because it helps create visuals that are easy to understand
		* Simple drag-and-drop feature lets users create interactive graphs ain dashboards and worksheets
	* Looker can give stakeholders a complete picture of the work by showing them visualization data and the actual data related to it
		* Communicates directly with a database, allowing for the connection of the data to the visual tool of choice
	* Spreadsheets:
		* Software applications
		* Structure data in a row/column format
		* Organize information in cells
		* Provide access to a limited amount of data
		* Manual data entry
		* Generally one user at a time
		* Controlled by the user
	* Databases:
		* Data stores -- access using a query language
		* Structure data using rules and relationships
		* Organize information in complex collections
		* Provide access to huge amounts of data
		* Strict and consistent data entry
		* Multiple users
		* Controlled by a database management system

## References
1. 