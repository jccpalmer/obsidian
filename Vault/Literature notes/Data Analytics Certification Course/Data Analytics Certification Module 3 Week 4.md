# Data Analytics Certification Module 3 Week 4
Created: 2022-09-26 at 13:49

## Effectively organize data
- Benefits of organizing data
	- Makes it easier to find and use
	- Helps avoid making mistakes during analysis
	- Helps to protect the data
- Best practices when organizing data
	- Naming conventions
	- Foldering
	- Archiving older files
	- Align naming and storage practices with the team
	- Develop metadata practices
- Naming conventions: Consistent guidelines that describe the content, date, or version of a file in its name
- Use logical and descriptive names in files to make them easier to find and use
- Foldering organizes files into folders to help keep project-related files together in one place
- Think about often copies of data are made and how they're stored in different places
- Relational databases can cut down on data duplication and store data more efficiently
- Best practices for file naming conventions:
	- Work out and agree on file naming conventions early on in a project to avoid renaming files again and again
	- Align your file naming with your team's or company's existing file-naming conventions
	- Ensure that your file names are meaningful; consider including information like project name and anything else that will help you quickly identify (and use) the file for the right purpose
	- Include the date and version number in file names; common formats are YYYYMMDD for dates and v## for versions (or revisions)
	- Create a text file as a sample file with content that describes (breaks down) the file naming convention and a file name that applies it
	- Avoid spaces and special characters in file names. Instead, use dashes, underscores, or capital letters. Spaces and special characters can cause errors in some applications
- Best practices for keeping files organized
	- Create folders and subfolders in a logical hierarchy so related files are stored together
	- Separate ongoing from completed work so your current project files are easier to find. Archive older files in a separate folder, or in an external storage location
	- If your files aren't automatically backed up, manually back them up often to avoid losing important work
- File naming do's
	- Work out your conventions early
	- Align file naming with your team
	- Make sure file names are meaningful
	- Keep file names short and sweet
	- Format dates YYYYMMDD
	- Lead revision numbers with 0
	- Use hyphens, underscores, of capital letters instead of using spaces

## Securing data
- Data security: Protecting data from unauthorized access or corruption by adopting safety measures
- Sheets and Excel allow you to protect spreadsheets or parts of spreadsheets from being edited; they also have access control features
	- Access control gives more control who can do what with a spreadsheet
- Excel can encrypt spreadsheets
- Encryption: The use of a unique algorithm to alter data and make it unusable by users and applications that don't know the algorithm
- Tokenization: The replacement of data elements with randomly generated data referred to as tokens
	- The original data is stored elsewhere and is mapped to the tokens

## References
1. [[Analytics references and resources]]