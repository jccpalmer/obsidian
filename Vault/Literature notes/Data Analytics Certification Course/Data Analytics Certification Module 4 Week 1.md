# Data Analytics Certification Module 4 Week 1
Created: 2022-09-27 at 16:25

## Focus on integrity
- Clean data is essential for the analysis process

## Data integrity and analytics objectives
- A strong analysis depends on the integrity of the data
- Compromised data leads to weak analyses
- Data integrity: The accuracy, completeness, consistency, and trustworthiness of data throughout its lifecycle
- Data replication: The process of storing data in multiple locations
- Data transfer: The process of copying data from storage device to memory, or from one computer to another
- Data manipulation: The process of changing data to make it more organized and easier to read
- Data manipulation is meant to make the data analysis process more efficient
	- An error in the process can compromise that efficiency
- Other threats to data integrity:
	- Human error
	- Viruses
	- Malware
	- Hacking
	- System failures
- Data engineering or the data warehouse ensure data integrity, but the analyst should still check
- Check that data is complete and valid before analysis
	- This will ensure that conclusions are accurate
- Data constraints:
	- Data type: Values must be a certain type
	- Data range: Values must fall between predefined maximum and minimum values
	- Mandatory: Values can be left blank or empty
	- Unique: Values can't have a duplicate
	- Regular expression (regex) patterns: Values must match a prescribed pattern
	- Cross-field validation: Certain conditions for multiple fields must be satisfied
	- Primary-key: Value must be unique per column
	- Set-membership: Values for a column must come from a set of discrete values
	- Foreign-key: Values for a column must be unique values coming a column in another table
	- Accuracy: The degree to which the data confirms to the actual entity being measured or described
	- Completeness: The degree to which the data contains all desired components or measures
	- Consistency: The degree to which the data is repeatable from different points of entry or collection
- It's important to check that the data used aligns with the business objective
- Learn to deal with data issues while staying focused on the objective
- Good alignment means that the data is relevant and can help you solve a business problem or determine a course of action to achieve a given business objective
- Master VLOOKUP and DATEDIF in Excel
- Data alignment takeaways:
	- When there is clean data and good alignment, you can get accurate insights and make conclusions the data supports
	- When there is clean data and good alignment, you can get accurate insights and make conclusions the data supports
	- If the data only partially aligns with an objective, think about how you could modify the objective, or use data constraints to make sure that the subset of data better aligns with the business objective

## Overcoming the challenges of insufficient data
- Types of insufficient data:
	- Data from only one source
	- Data that keeps updating
	- Outdated data
	- Geographically-limited data
- Ways to address insufficient data
	- Identify trends with the available data
	- Wait for more data if time allows
	- Talk with stakeholders and adjust your objective
	- Look for a new dataset
- Data issues
	- No data
		- Solution: 
			- Gather the data on a small scale to perform a preliminary analysis and then request additional time to complete the analysis after you have collected more data.
			- If there isn’t time to collect data, perform the analysis using proxy data from other datasets.  _This is the most common workaround._
	- Too little data
		- Solution:
			- Do the analysis using proxy data along with actual data.
			- Adjust your analysis to align with the data you already have.
	- Wrong data, including data with errors
		- Solution:
			- If you have the wrong data because requirements were misunderstood, communicate the requirements again.
			- Identify errors in the data and, if possible, correct them at the source by looking for a pattern in the errors.
			- If you can’t correct data errors yourself, you can ignore the wrong data and go ahead with the analysis if your sample size is still large enough and ignoring the data won’t cause systematic bias.
- Using 100% of the population is costly and time-consuming
- Sample size: A part of a population that is representative of the population
- The sample size helps ensure the degree to which you can be confident that your conclusions accurately represent the population
- Small samples can lead to uncertainty
- Sampling bias: A sample isn't representative of the population as a whole
- Random sampling can help address issues with sampling bias
- Random sampling: A way of selecting a sample from a population so that every possible type of the sample has an equal chance of being chosen
- Data analysts usually come in after the sample has been collected
- Margin of error: The difference between the sample's results and the expected population results
- Confidence level: The probability that your sample size accurately reflects the greater population
- Confidence interval: The range of possible values that the population's result would be at the confidence level of the study
- Statistical insignificance: The determination of whether the result could be due to random chance or not
- Don't use a sample size less than 30
- 95% is the most commonly used confidence level
	- 90% works in some cases
	- 99% is preferred
- Larger sample sizes have higher costs

## Testing your data
- Statistical power: The probability of getting meaningful results from a test
- Hypothesis testing: A way to see if a survey or experiment has meaningful results
- Stat power is usually shown as a value between 0 and 1
- If a test is statistically significant, it means the results of the test are real and not an error caused by random chance
- 0.8 or 80% is typically considered the minimum for stat significance
- Sample size calculator needs:
	- Population size
	- Confidence level (%)
	- Margin of error (%)
- Estimated reponse rate: The percentage of people expected to complete the survey
- In a sample size calculator, confidence level and margin of error are mutually exclusive

## Consider the margin of error
- Margin of error: The maximum amount that the sample results are expected to differ from those of the actual population
- The margin of error is counted in both directions of the sample results
- Larger samples are more representative of the population
- Decreasing confidence level would also work, but would also more likely make the survey less accurate
- To calculate margin of error:
	- Population size
	- Sample size
	- Confidence level

## References
1. [[Analytics references and resources]]