# Data Analytics Certification Module 3 Week 1
Created: 2022-09-21 at 18:26

## Collecting data
- Every piece of information is data
- Data collection must be done in regards to ethics, rights, and privacy
- How data is collected:
	- Surveys
	- Interviews
	- Observations
	- Forms
	- Questionnaires
	- Cookies
- Knowing how data is generated helps adds context
- Knowing how to collect data can make the data analysis process more efficient
- Data collection considerations:
	- How the data will be collected
	- Choose data sources
	- Decide what data to use
	- How much data to collect
	- Select the right data type
	- Determine the time frame
- First-party data: Data collected by an individual or group using their own resources
- Second-party data: Data collected by a group directly from its audience and then sold
- Third-party data: Data collected from outside sources who did not collect it directly
- All data, regardless of source, needs to be tested for accuracy and trustworthiness
- Population: All possible data values in a certain dataset
- Sample: A part of a population that is representative of the population 

## Differentiate between data formats and structures
- Discrete data: Data that is counted and has a limited number of values
- Continuous data: Data that is measured and can have almost any numeric value
- Nominal data: A type of qualitative data that is categorized without a set order
- Ordinal data: A type of qualitative data with a set order or scale
- Internal data: Data that lives within a company's own systems
- External data: Data that lives and is generated outside of an organization
- Structured data: Data organized in a certain format such as rows and columns
- Unstructured data: Data that is not organized in any easily identifiable manner
- Internal data is usually more reliable and easier to collect
- External data is valuable when the analysis depends on as many sources as possible
- Spreadsheets and relational databases are two examples of software-based structured data
- Audio and video files, emails, photos, social media are examples of unstructured data
- Data model: A model that is used for organizing data elements and how they relate to one another
- Data elements: Pieces of information, such as names, account numbers, and addresses
- Data modeling is the process of creating diagrams that visually represent how data is organized and structured
	- Conceptual: High-level view of the data structure, such as how data interacts across an organization
	- Logical: Focus on the technica details of a database, such as relationships, attributes, and entities
	- Physical: Depiction of how a database operates, defining all entities and attributes used
- Entity Relationship Diagram (ERD): A visual way to understand the relationship between entities in a data model
- Unified Modeling Language (UML): Detailed diagrams that describe the structure of a system by showing the system's entities, attributes, operations, and their relationships

## Explore data types, fields, and values
- Data type: A specific kind of data attribute that tells what kind of value the data is
	- SQL allows for different data types depending on which database you're using
- Data types in spreadsheets:
	- Number
	- Text or string
	- Boolean
- Text or string type: A sequences of characters and punctuation that contains textual information
- Boolean type: A data type with only two possible values, such as TRUE or FALSE
- Boolean logic:
	- AND: Breaks down the logic of the statement to filter results based on two options
	- OR: Breaks down the logic of the statement to filter based either of the two conditions
	- NOT: Breaks down the logic of the statements to filter by subtracting specific conditions from the results
- Booleans can be combined into complex statements
- Tabular data is arranged in rows and columns 
	- Rows (spreadsheet) = Records
	- Columns (spreadsheet) = Fields
- Each record has several fields
- Wide data: Data in which every data subject has a single row with multiple columns to hold the values of various attributes of the subject
- Long data: Data in which each row is one time point per subject, so each subject will have data in multiple rows
- Data transformation: The process of changing the data's format, structure, or values
- Data transformation usually includes:
	- Adding, copying, or replicating the data
	- Deleting fields or records
	- Standardizing the names of variables
	- Renaming, moving, or combining columns in a database
	- Joining one set of data with another
	- Saving a file in a different format
- Why transform data?
	- Organization
	- Compatibility
	- Migration
	- Merging
	- Enhancement
	- Comparison

## References
1. [Kaggle](https://www.kaggle.com/)