# Data Analytics Certification Module 1 Week 4
Created: 2022-08-05 at 08:02

## The ins and outs of core data tools
### Spreadsheets
* Attribute: A characteristic or quality of data used to label a column in a table
* Observation: All of the attributes for something contained in a row of a data table

### SQL
* SQL can do most things spreadsheets can do
	* SQL is sort of like an oversized spreadsheet
		* It can store, organize, and analyze data
* Query: A request for data or information from a database
* Queries allow for selecting specific data from a table and filtering the data based on certain conditions and criteria
* Using an asterisk tells the database to call up the needed table
* Database examples:
	* Oracle
	* MySQL
	* PostgreSQL
	* MS SQL Server
* SQL queries are universal
* The WHERE statement is used to filter data based on certain conditions
* Syntax is the predetermined structure of a language that includes all required words, symbols, and punctuation, as well as their proper placement
* SQL syntact is the same:
	* SELECT: Choose the columns you want to return
		* Enter the table columns
	* FROM: Choose the tables where the columns you want are located
		* Enter the table name
	* WHERE: Filter for certain information
		* Add the conditions for the query
* EX:
```
SELECT
	customer_id,
	first_name,
	last_name
FROM
	customer_data.customer_name
WHERE
	customer_id > 0
	AND first_name = 'Jordan'
	AND last_name = 'Palmer'
```
* WHERE uses 'AND' (as well as 'OR' or 'NOT') statement to connect conditions, whereas SELECT uses commas
* Capitalization and indentation aren't necessary, but helpful for visualization and finding errors
* A semicolon terminates the statement, though it's not enforced with all SQL databases
* LIKE clause is powerful because it allows for telling the database to look for a certain pattern
	* The % is used as a wildcard to match one or more characters
		* Some databases use * as a wildcard
* Use * sparingly in the SELECT clause
	* It could return a tremendous amount of data
* Comments help you remember what the name of the column means or represents
	* Comments are surrounded by either
```
	/* */
	--
```
* Comments can also be added outside of a statement as well as within one
	* This flexibility allows you to provide an overall description of the action, step-by-step notes about how to achieve it, and why you set different paramets or conditions
```
-- Pull basic information from the customer table
SELECT
	customer_id, -- main ID used to join with customer_address
	first_name, -- customer's first name from loyalty program
	last_name -- customer's last name
FROM
	customer_data.customer_name
```
* Do not rely on # for comments
	* MySQL for example doesn't recognize #
	* This isn't bash, YAML, etc
* Typically go for -- for comments
* Aliases assign a new name to the columns or table to make them easier to work with and avoid the need for comments
	* uses the AS clause
	* Good for the duration of the query only
	* Doesn't change the actual name of a column or table in the database
```
field1 AS last_name
table AS customers

SELECT
	last_name
FROM
	customers
WHERE
	last_name LIKE 'Ch%';
```
* <> = does not equal

### Visualizations
* Visualizations make data easier to digest for an analyst's audience
* Data visualization steps
	* Explore the data for patterns
	* Plan the visuals
	* Create the visuals
* Use the visualization tools in the spreadsheet program or Tableau, or RStudio if using R
* Tableau lets you pull in data from nearly any system and turn it into compelling visuals or actionable insights
	* It offers built-in visual best practices, allowing for fast, easy, and useful data sharing or analyzation
	* Includes an interactive dashboard to explore data interactively
* RStudio is an IDE for R
* 

## References
1. [[Analytics references and resources#^3cc5fc | SQL Tutorial]]
2. [[Analytics references and resources#^681b59 | SQL Cheat Sheet]]
3. [[Analytics references and resources#^a8fcba | Tableau How-Tos]]
4. [[Analytics references and resources#^6fb51e | RStudio Cheat Sheets]]
5. 