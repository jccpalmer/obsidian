# Data Analytics Certification Module 1 Week 1
Created: 2022-08-05 at 07:59

## Introducing data analytics
* People analyze data all the time
* Data is contantly being created
* Data is a collection of facts
* Data analysis is the collection, transformation, and organization of data to draw conclusions, make predictions, and drive informed decision making
* Data analytics can help organizations completely rethink something they do or point them in a totally new direction
* Six steps:
	* Ask questions and define the problem
	* Prepare data by collecting and storing the information
	* Process data by cleaning and checking the information
	* Analyze data to find patterns, relationships, and trends
	* Share data with your audience
	* Act on the data and use the analysis results
* Six steps apply to any data analysis
* Data analysts are responsible for the ethical collection and use of data
* To be ethical, one step to take is to ensure that a minimum amount of people have access to the raw data
* Similarly, ensure that only relevant parties get access to the analysis and aggregate data
* The excellence of statistics is rigor; the excellence of an analyst is speed; performance is one of the machine learning and artificial intelligence engineers’ goals
* Data ecosystems: the various elements that interact with one another in order to produce, manage, store, organize, analyze, and share data
* Data science: creating new ways of modeling and understanding the unknown using raw data
* Data analysis: the collection, transformation, and organization of data to draw conclusions, make predictions, and drive informed decision making
* Data analytics is the science of data
* Data-driven decision making: using facts to guide business strategy
* A data analyst finds data, analyzes it, and uses it to uncover trends, patterns, and relationships
* Sometimes the data-driven strategy will build on what has worked in the past
	* Other times, it can guide a business to branch out in a whole new direction.
* Questions to ask to find a balance between data and intuition
	* What kind of results are needed?
	* Who will be informed?
	* Am I answering the question being asked?
	* How quickly does a decision need to be made?
* Dataset: A collection of data that can be manipulated or analyzed as one unit

## References
1. 