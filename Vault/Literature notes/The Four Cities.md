# The Four Cities
Created: 2022-03-08 at 10:08

Findrias:
- East
- Cloud-fair
- Sword of Light
- Ogma, Splendour of the Sun

Gorias:
- South
- Flame-bright
- Spear of Victory
- Nuada, Wielder of the White Light

Murias:
- West
- Stillness of deep waters
- Cauldron of Plenty
- Dagda, the Green Harper

Falias:
- North
- Steafastness of adamant
- Stone of Destiny
- Midyir, the Red-Maned

## References
1. [[The Earth-Shapers]]