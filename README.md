# Obsidian vault backup

This is my git backup of my Obsidian vault. This gets updated frequently as I develop my second brain.

I plan to set up an automatic job that will sync the git changes, but that will require some research first.
